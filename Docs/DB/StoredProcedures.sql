﻿DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_common_code_detail`(
	IN inMasterCode	VARCHAR(20)
)
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    
    SELECT `DetailCode`,
		`CodeText`
	FROM `common_code_detail`
    WHERE 1=1
		AND MasterCode = inMasterCode
        AND UseYN = 'Y'
    ;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_common_notice`()
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT * FROM common_notice;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_common_notice_insert`(
	IN inSeq	INT,
    IN inVersion	VARCHAR(100),
	IN inMemo	LONGTEXT
)
BEGIN

	START TRANSACTION;
    
	SET inSeq = 0;
    
	INSERT INTO `common_notice`
	(`Seq`,
    `Version`,
    `Contents`,
	`ModDate`)
	VALUES
	(inSeq,
    inVersion,
	inMemo,
	NOW())
    ON DUPLICATE KEY UPDATE
    `Version` = inVersion,
    `Contents` = inMemo,
    `ModDate` = NOW();

	COMMIT;
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_common_user_master_check`(
	IN inSelectUserID VARCHAR(20)
)
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT * FROM `common_user_master` WHERE UserID = inSelectUserID;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_common_user_master_delete`(
	IN inSelectUserID VARCHAR(20)
)
BEGIN

	START TRANSACTION;

	DELETE FROM `device_realtime`.`common_user_master`
	WHERE `UserID` = inSelectUserID;

	COMMIT;
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_common_user_master_insert`(
	IN inUserID VARCHAR(20),
    IN inSelectUserID VARCHAR(20),
    IN inPassWord VARCHAR(45),
    IN inName VARCHAR(10),
    IN inAuthCode VARCHAR(20),
    IN inValue1 VARCHAR(200),
    IN inValue2 VARCHAR(200),
    IN inValue3 VARCHAR(200),
    IN inUseYN VARCHAR(1),
    IN inPWDMode VARCHAR(1)
)
BEGIN

	START TRANSACTION;

	INSERT INTO `device_realtime`.`common_user_master`
	(`UserID`,
	`UserPW`,
	`AuthCode`,
	`Name`,
	`Value1`,
	`Value2`,
	`Value3`,
	`InsDate`,
	`InsUserID`,
	`ModDate`,
	`ModUserID`,
	`UseYN`)
	VALUES
	(inSelectUserID,
    PASSWORD(inPassWord),
	inAuthCode,
	inName,
	inValue1,
	inValue2,
	inValue3,
	NOW(),
	inUserID,
	NOW(),
	inUserID,
	inUseYN)
    ON DUPLICATE KEY UPDATE
    `UserPW` = CASE
				WHEN inPWDMode = 1 THEN PASSWORD(inPassWord)
				ELSE `UserPW`
				END,
	`AuthCode` = inAuthCode,
	`Name` = inName,
	`Value1` = inValue1,
	`Value2` = inValue2,
	`Value3` = inValue3,
	`ModDate` = NOW(),
	`ModUserID` = inUserID,
	`UseYN` = inUseYN;

	COMMIT;
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_common_user_master_login`(
	IN inUserID VARCHAR(20),
    IN inPassword VARCHAR(20)
)
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT `UserID`, `AuthCode` FROM `common_user_master` WHERE UserID = inUserID AND `UserPW` = PASSWORD(inPassword);

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_common_user_master_select`()
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT cum.`UserID`,
		PASSWORD(cum.`UserPW`) AS UserPW,
		cum.`AuthCode`,
        ccd.`CodeText` AS AuthCodeText,
		cum.`Name`,
		cum.`Value1`,
		cum.`Value2`,
		cum.`Value3`,
		cum.`Value4`,
		cum.`Value5`,
		cum.`InsDate`,
		cum.`InsUserID`,
		cum.`ModDate`,
		cum.`ModUserID`,
		cum.`UseYN`
	FROM `common_user_master` cum
		LEFT JOIN `common_code_detail` ccd
			ON ccd.DetailCode = cum.AuthCode
    ;


END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_common_user_password_reset`(
	IN inUserID VARCHAR(20),
    IN inPassword VARCHAR(20)
)
BEGIN
	
    START TRANSACTION;

	UPDATE `common_user_master`
	SET
		`UserPW` = PASSWORD(inPassword)
	WHERE `UserID` = inUserID;

	COMMIT;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_acprate_10kw_select`(
	IN inYear VARCHAR(10)
)
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT fa10.m1, fa10.m2, fa10.m3, fa10.m4, fa10.m5, fa10.m6, fa10.m7, fa10.m8, fa10.m9, fa10.m10, fa10.m11, fa10.m12
	FROM fc_acprate_10kw fa10
    WHERE fa10.`Year` = inYear;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_acprate_1kw_select`(
	IN inYear VARCHAR(10)
)
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT fa1.m1, fa1.m2, fa1.m3, fa1.m4, fa1.m5, fa1.m6, fa1.m7, fa1.m8, fa1.m9, fa1.m10, fa1.m11, fa1.m12
	FROM fc_acprate_1kw fa1
    WHERE fa1.`Year` = inYear;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_acprate_5kw_select`(
	IN inYear VARCHAR(10)
)
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT fa5.m1, fa5.m2, fa5.m3, fa5.m4, fa5.m5, fa5.m6, fa5.m7, fa5.m8, fa5.m9, fa5.m10, fa5.m11, fa5.m12
	FROM fc_acprate_5kw fa5
    WHERE fa5.`Year` = inYear;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_fc_cache_insert`(
	IN inDataSeq BIGINT
	,IN inDeviceNum INT
	,IN inDate VARCHAR(50)
	,IN inTime VARCHAR(50)
	,IN inPattern VARCHAR(50)
	,IN inLoad VARCHAR(50)
	,IN inACP VARCHAR(50)
	,IN inTTCogen VARCHAR(50)
	,IN inSeq VARCHAR(50)
	,IN inTT117 VARCHAR(50)
	,IN inTT102 VARCHAR(50)
	,IN inTT105 VARCHAR(50)
	,IN inTT109 VARCHAR(50)
	,IN inTT112 VARCHAR(50)
	,IN inTT123 VARCHAR(50)
	,IN inTT120 VARCHAR(50)
	,IN inTT101 VARCHAR(50)
	,IN inTT301 VARCHAR(50)
	,IN inTT302 VARCHAR(50)
	,IN inTT01 VARCHAR(50)
	,IN inTT02 VARCHAR(50)
	,IN inTT03 VARCHAR(50)
	,IN inTT04 VARCHAR(50)
	,IN inTT05 VARCHAR(50)
	,IN inTT06 VARCHAR(50)
	,IN inTT07 VARCHAR(50)
	,IN inTT08 VARCHAR(50)
	,IN inTT09 VARCHAR(50)
	,IN inTT10 VARCHAR(50)
	,IN inTT11 VARCHAR(50)
	,IN inTT12 VARCHAR(50)
	,IN inTT13 VARCHAR(50)
	,IN inTT14 VARCHAR(50)
	,IN inTT15 VARCHAR(50)
	,IN inTT16 VARCHAR(50)
	,IN inTT201 VARCHAR(50)
	,IN inTTCold VARCHAR(50)
	,IN inTTHot VARCHAR(50)
	,IN inTTA VARCHAR(50)
	,IN inPIT101 VARCHAR(50)
	,IN inPIT_NG VARCHAR(50)
	,IN inPIT201 VARCHAR(50)
	,IN inPIT301 VARCHAR(50)
	,IN inP101_SP VARCHAR(50)
	,IN inP101_MF VARCHAR(50)
	,IN inP102_SP VARCHAR(50)
	,IN inP201_SP VARCHAR(50)
	,IN inP201_MF VARCHAR(50)
	,IN inP202_SP VARCHAR(50)
	,IN inP202_MF VARCHAR(50)
	,IN inP203_SP VARCHAR(50)
	,IN inP203_MF VARCHAR(50)
	,IN inP301_SP VARCHAR(50)
	,IN inP302_SP VARCHAR(50)
	,IN inP303_SP VARCHAR(50)
	,IN inP304_SP VARCHAR(50)
	,IN inH2_Leak VARCHAR(50)
	,IN inP101_MFC_SP VARCHAR(50)
	,IN inP101_MFC_MF VARCHAR(50)
	,IN inP102_MFC_SP VARCHAR(50)
	,IN inP102_MFC_MF VARCHAR(50)
	,IN inH2_Leak_2 VARCHAR(50)
	,IN inCogen VARCHAR(50)
	,IN inP301_MF VARCHAR(50)
	,IN inStack_V VARCHAR(50)
	,IN inINV_V VARCHAR(50)
	,IN inINV_I VARCHAR(50)
	,IN inAC_V VARCHAR(50)
	,IN inStack_I VARCHAR(50)
	,IN inAC_V_A VARCHAR(50)
	,IN inAC_V_B VARCHAR(50)
	,IN inAC_V_C VARCHAR(50)
	,IN inINV_Error VARCHAR(50)
	,IN inLS303_H VARCHAR(50)
	,IN inLS303_L VARCHAR(50)
	,IN inLS304_H VARCHAR(50)
	,IN inLS304_L VARCHAR(50)
	,IN inCVM VARCHAR(50)
	,IN inNC101 VARCHAR(50)
	,IN inNC102 VARCHAR(50)
	,IN inNC103 VARCHAR(50)
	,IN inNC104 VARCHAR(50)
	,IN inNC105 VARCHAR(50)
	,IN inNC106 VARCHAR(50)
	,IN inNC202 VARCHAR(50)
	,IN inNC308 VARCHAR(50)
	,IN inNC309 VARCHAR(50)
	,IN inNC311 VARCHAR(50)
	,IN inNC312 VARCHAR(50)
	,IN inNC313 VARCHAR(50)
	,IN inNC104_AB VARCHAR(50)
	,IN inNC303 VARCHAR(50)
	,IN inNC307 VARCHAR(50)
	,IN inIgniter VARCHAR(50)
	,IN inPWR_RLY VARCHAR(50)
	,IN inACDC VARCHAR(50)
	,IN inDCDC VARCHAR(50)
	,IN inStack_RV VARCHAR(50)
	,IN inW_TT101 VARCHAR(50)
	,IN inW_TT102 VARCHAR(50)
	,IN inW_TT109 VARCHAR(50)
	,IN inW_TT117 VARCHAR(50)
	,IN inW_TT120 VARCHAR(50)
	,IN inW_TT123 VARCHAR(50)
	,IN inW_TT301 VARCHAR(50)
	,IN inW_TT302 VARCHAR(50)
	,IN inW_FAN VARCHAR(50)
	,IN inW_PITNG VARCHAR(50)
	,IN inW_PIT101 VARCHAR(50)
	,IN inW_PIT201 VARCHAR(50)
	,IN inW_PIT301 VARCHAR(50)
	,IN inW_OCV VARCHAR(50)
	,IN inW_StackV VARCHAR(50)
	,IN inW_CVM VARCHAR(50)
	,IN inW_GRID VARCHAR(50)
	,IN inW_Inverter VARCHAR(50)
	,IN inW_InvCOMM VARCHAR(50)
	,IN inW_Igniter VARCHAR(50)
	,IN inW_TempCOM VARCHAR(50)
	,IN inW_NC303 VARCHAR(50)
	,IN inW_H2Leak VARCHAR(50)
	,IN inW_STACK VARCHAR(50)
	,IN inW_P101 VARCHAR(50)
	,IN inW_P102 VARCHAR(50)
	,IN inW_P201 VARCHAR(50)
	,IN inW_P202 VARCHAR(50)
	,IN inW_P203 VARCHAR(50)
	,IN inW_P301 VARCHAR(50)
	,IN inW_P302 VARCHAR(50)
	,IN inW_TT05 VARCHAR(50)
	,IN inW_TT12 VARCHAR(50)
	,IN inW_TT13 VARCHAR(50)
	,IN inW_TT15 VARCHAR(50)
	,IN inW_TT04 VARCHAR(50)
	,IN inW_TT16 VARCHAR(50)
	,IN inW_TTA VARCHAR(50)
	,IN inW_TT14 VARCHAR(50)
	,IN inS_TT101 VARCHAR(50)
	,IN inS_TT102 VARCHAR(50)
	,IN inS_TT109 VARCHAR(50)
	,IN inS_TT117 VARCHAR(50)
	,IN inS_TT120 VARCHAR(50)
	,IN inS_TT123 VARCHAR(50)
	,IN inS_TT301 VARCHAR(50)
	,IN inS_TT302 VARCHAR(50)
	,IN inS_FAN VARCHAR(50)
	,IN inS_PITNG VARCHAR(50)
	,IN inS_PIT101 VARCHAR(50)
	,IN inS_PIT201 VARCHAR(50)
	,IN inS_PIT301 VARCHAR(50)
	,IN inS_OCV VARCHAR(50)
	,IN inS_StackV VARCHAR(50)
	,IN inS_CVM VARCHAR(50)
	,IN inS_GRID VARCHAR(50)
	,IN inS_Inverter VARCHAR(50)
	,IN inS_InvCOMM VARCHAR(50)
	,IN inS_Igniter VARCHAR(50)
	,IN inS_TempCOM VARCHAR(50)
	,IN inS_NC303 VARCHAR(50)
	,IN inS_H2Leak VARCHAR(50)
	,IN inS_STACK VARCHAR(50)
	,IN inS_P101 VARCHAR(50)
	,IN inS_P102 VARCHAR(50)
	,IN inS_P201 VARCHAR(50)
	,IN inS_P202 VARCHAR(50)
	,IN inS_P203 VARCHAR(50)
	,IN inS_P301 VARCHAR(50)
	,IN inS_P302 VARCHAR(50)
	,IN inS_TT05 VARCHAR(50)
	,IN inS_TT12 VARCHAR(50)
	,IN inS_TT13 VARCHAR(50)
	,IN inS_TT15 VARCHAR(50)
	,IN inS_TT04 VARCHAR(50)
	,IN inS_TT16 VARCHAR(50)
	,IN inS_TTA VARCHAR(50)
	,IN inS_TT14 VARCHAR(50)
    ,IN inDeviceStatus VARCHAR(50)
    
    ,IN inLANSTATE VARCHAR(50)
	,IN inConduct VARCHAR(50)
	,IN inRoomCon VARCHAR(50)
    
	,IN inW_Level_Con VARCHAR(50)
	,IN inW_Desulfur VARCHAR(50)
	,IN inW_CO VARCHAR(50)
	,IN inW_Backflow VARCHAR(50)
	,IN inW_P301_F_Cnt VARCHAR(50)
    
	,IN inS_Level_Con VARCHAR(50)
	,IN inS_Desulfur VARCHAR(50)
	,IN inS_CO VARCHAR(50)
	,IN inS_Backflow VARCHAR(50)
	,IN inS_P301_F_Cnt VARCHAR(50)
    
	,IN inNC302 VARCHAR(50)
    
	,IN inW_P201OL VARCHAR(50)
	,IN inW_TTINT VARCHAR(50)
	,IN inW_BOPI VARCHAR(50)
	,IN inW_Conduct VARCHAR(50)
	,IN inW_StackI VARCHAR(50)
	,IN inW_NGPump VARCHAR(50)
	,IN inW_TT03 VARCHAR(50)
	,IN inW_TT201 VARCHAR(50)
	,IN inW_P203OL VARCHAR(50)
    
	,IN inS_P201OL VARCHAR(50)
	,IN inS_TTINT VARCHAR(50)
	,IN inS_BOPI VARCHAR(50)
	,IN inS_Conduct VARCHAR(50)
	,IN inS_StackI VARCHAR(50)
	,IN inS_NGPump VARCHAR(50)
	,IN inS_TT03 VARCHAR(50)
	,IN inS_TT201 VARCHAR(50)
	,IN inS_P203OL VARCHAR(50)
)
BEGIN

	DECLARE maxDataSeq BIGINT;
	SET maxDataSeq = DATE_FORMAT(date_add(now(), interval -3 month), "%Y%m%d%H%i%s");

	/*
	SET @sqlString1 = CONCAT('DELETE FROM fc_', inDeviceNum,'_backup WHERE DataSeq < ', maxDataSeq, ';');

	START TRANSACTION;

	PREPARE sqlString1 FROM @sqlString1;
    EXECUTE sqlString1;
    
    COMMIT;
	*/
	SET @sqlString2 = CONCAT(
		'INSERT INTO `device_storage`.`fc_', inDeviceNum,'` (`DataSeq`,
		`DeviceNum`,
		`Date`,
		`Time`,
		`Pattern`,
		`Load`,
		`ACP`,
		`TTCogen`,
		`Seq`,
		`TT117`,
		`TT102`,
		`TT105`,
		`TT109`,
		`TT112`,
		`TT123`,
		`TT120`,
		`TT101`,
		`TT301`,
		`TT302`,
		`TT01`,
		`TT02`,
		`TT03`,
		`TT04`,
		`TT05`,
		`TT06`,
		`TT07`,
		`TT08`,
		`TT09`,
		`TT10`,
		`TT11`,
		`TT12`,
		`TT13`,
		`TT14`,
		`TT15`,
		`TT16`,
		`TT201`,
		`TTCold`,
		`TTHot`,
		`TTA`,
		`PIT101`,
		`PIT_NG`,
		`PIT201`,
		`PIT301`,
		`P101_SP`,
		`P101_MF`,
		`P102_SP`,
		`P201_SP`,
		`P201_MF`,
		`P202_SP`,
		`P202_MF`,
		`P203_SP`,
		`P203_MF`,
		`P301_SP`,
		`P302_SP`,
		`P303_SP`,
		`P304_SP`,
		`H2_Leak`,
		`P101_MFC_SP`,
		`P101_MFC_MF`,
		`P102_MFC_SP`,
		`P102_MFC_MF`,
		`H2_Leak_2`,
		`Cogen`,
		`P301_MF`,
		`Stack_V`,
		`INV_V`,
		`INV_I`,
		`AC_V`,
		`Stack_I`,
		`AC_V_A`,
		`AC_V_B`,
		`AC_V_C`,
		`INV_Error`,
		`LS303_H`,
		`LS303_L`,
		`LS304_H`,
		`LS304_L`,
		`CVM`,
		`NC101`,
		`NC102`,
		`NC103`,
		`NC104`,
		`NC105`,
		`NC106`,
		`NC202`,
		`NC308`,
		`NC309`,
		`NC311`,
		`NC312`,
		`NC313`,
		`NC104_AB`,
		`NC303`,
		`NC307`,
		`Igniter`,
		`PWR_RLY`,
		`AC/DC`,
		`DC/DC`,
		`Stack_RV`,
		`S_TT14`,
        `DeviceStatus`,
        `LANSTATE`,
		`Conduct`,
		`RoomCon`,
		`W_Level_Con`,
		`W_Desulfur`,
		`W_CO`,
		`W_Backflow`,
		`W_P301_F_Cnt`,
		`S_Level_Con`,
		`S_Desulfur`,
		`S_CO`,
		`S_Backflow`,
		`S_P301_F_Cnt`,
		`NC302`,
		`W_P201OL`,
		`W_TTINT`,
		`W_BOPI`,
		`W_Conduct`,
		`W_StackI`,
		`W_NGPump`,
		`W_TT03`,
		`W_TT201`,
		`W_P203OL`,
		`S_P201OL`,
		`S_TTINT`,
		`S_BOPI`,
		`S_Conduct`,
		`S_StackI`,
		`S_NGPump`,
		`S_TT03`,
		`S_TT201`,
		`S_P203OL`)'
		,'VALUES('
		,inDataSeq, ', '
		,inDeviceNum, ', '
		,'''',inDate, ''', '
		,'''',inTime, ''', '
		,inPattern, ', '
		,inLoad, ', '
		,inACP, ', '
		,inTTCogen, ', '
		,inSeq, ', '
		,inTT117, ', '
		,inTT102, ', '
		,inTT105, ', '
		,inTT109, ', '
		,inTT112, ', '
		,inTT123, ', '
		,inTT120, ', '
		,inTT101, ', '
		,inTT301, ', '
		,inTT302, ', '
		,inTT01, ', '
		,inTT02, ', '
		,inTT03, ', '
		,inTT04, ', '
		,inTT05, ', '
		,inTT06, ', '
		,inTT07, ', '
		,inTT08, ', '
		,inTT09, ', '
		,inTT10, ', '
		,inTT11, ', '
		,inTT12, ', '
		,inTT13, ', '
		,inTT14, ', '
		,inTT15, ', '
		,inTT16, ', '
		,inTT201, ', '
		,inTTCold, ', '
		,inTTHot, ', '
		,inTTA, ', '
		,inPIT101, ', '
		,inPIT_NG, ', '
		,inPIT201, ', '
		,inPIT301, ', '
		,inP101_SP, ', '
		,inP101_MF, ', '
		,inP102_SP, ', '
		,inP201_SP, ', '
		,inP201_MF, ', '
		,inP202_SP, ', '
		,inP202_MF, ', '
		,inP203_SP, ', '
		,inP203_MF, ', '
		,inP301_SP, ', '
		,inP302_SP, ', '
		,inP303_SP, ', '
		,inP304_SP, ', '
		,inH2_Leak, ', '
		,inP101_MFC_SP, ', '
		,inP101_MFC_MF, ', '
		,inP102_MFC_SP, ', '
		,inP102_MFC_MF, ', '
		,inH2_Leak_2, ', '
		,inCogen, ', '
		,inP301_MF, ', '
		,inStack_V, ', '
		,inINV_V, ', '
		,inINV_I, ', '
		,inAC_V, ', '
		,inStack_I, ', '
		,inAC_V_A, ', '
		,inAC_V_B, ', '
		,inAC_V_C, ', '
		,inINV_Error, ', '
		,inLS303_H, ', '
		,inLS303_L, ', '
		,inLS304_H, ', '
		,inLS304_L, ', '
		,inCVM, ', '
		,inNC101, ', '
		,inNC102, ', '
		,inNC103, ', '
		,inNC104, ', '
		,inNC105, ', '
		,inNC106, ', '
		,inNC202, ', '
		,inNC308, ', '
		,inNC309, ', '
		,inNC311, ', '
		,inNC312, ', '
		,inNC313, ', '
		,inNC104_AB, ', '
		,inNC303, ', '
		,inNC307, ', '
		,inIgniter, ', '
		,inPWR_RLY, ', '
		,inACDC, ', '
		,inDCDC, ', '
		,inStack_RV, ', '
		,inS_TT14, ', '
        ,inDeviceStatus, ', '
        ,inLANSTATE, ', '
		,inConduct, ', '
		,inRoomCon, ', '
		,inW_Level_Con, ', '
		,inW_Desulfur, ', '
		,inW_CO, ', '
		,inW_Backflow, ', '
		,inW_P301_F_Cnt, ', '
		,inS_Level_Con, ', '
		,inS_Desulfur, ', '
		,inS_CO, ', '
		,inS_Backflow, ', '
		,inS_P301_F_Cnt, ', '
		,inNC302, ', '
		,inW_P201OL, ', '
		,inW_TTINT, ', '
		,inW_BOPI, ', '
		,inW_Conduct, ', '
		,inW_StackI, ', '
		,inW_NGPump, ', '
		,inW_TT03, ', '
		,inW_TT201, ', '
		,inW_P203OL, ', '
		,inS_P201OL, ', '
		,inS_TTINT, ', '
		,inS_BOPI, ', '
		,inS_Conduct, ', '
		,inS_StackI, ', '
		,inS_NGPump, ', '
		,inS_TT03, ', '
		,inS_TT201, ', '
        ,inS_P203OL, ');');

	START TRANSACTION;

    PREPARE sqlString2 FROM @sqlString2;
    EXECUTE sqlString2;
    
    COMMIT;
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_master_check`(
	IN inDeviceNum VARCHAR(20)
)
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT * FROM `fc_master` WHERE DeviceNum = inDeviceNum;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_master_delete`(
	IN inDeviceNum VARCHAR(20)
)
BEGIN

	START TRANSACTION;

	DELETE FROM `device_realtime`.`fc_master`
	WHERE `DeviceNum` = inDeviceNum;

	COMMIT;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_master_gen_select`(
	IN inDeviceNum VARCHAR(20)
)
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT frg.`DeviceNum`,
		CASE 
			WHEN fm.Category = 'EC001' OR fm.Category = 'EC002' THEN IFNULL(frg.Total_H_gen,0) / 10
			WHEN fm.Category = 'EC003' OR fm.Category = 'EC004' THEN IFNULL(frg.Total_H_gen,0) 
		END
		AS Total_H_gen,
        CASE 
			WHEN fm.Category = 'EC001' OR fm.Category = 'EC002' THEN IFNULL(frg.Total_E_gen,0) / 10
			WHEN fm.Category = 'EC003' OR fm.Category = 'EC004' THEN IFNULL(frg.Total_E_gen,0) 
		END
		AS Total_E_Gen,
		frg.`m1`,
		frg.`m2`,
		frg.`m3`,
		frg.`m4`,
		frg.`m5`,
		frg.`m6`,
		frg.`m7`,
		frg.`m8`,
		frg.`m9`,
		frg.`m10`,
		frg.`m11`,
		frg.`m12`,
		frg.`LastYear`,
		frg.`TheYearBefore`
	FROM `fc_realtime_gen` frg
		LEFT JOIN `fc_master` fm
			ON fm.DeviceNum = frg.DeviceNum
    WHERE frg.`DeviceNum` = inDeviceNum;


END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_master_info_select`(
	IN inDeviceNum VARCHAR(20)
)
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT fri.`DeviceNum`,
		fri.`FWver`,
		fri.`End_day`
	FROM `fc_realtime_info` fri
    WHERE fri.`DeviceNum` = inDeviceNum;


END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_master_insert`(
	IN inDeviceNum VARCHAR(20),
    IN inCategory VARCHAR(10),
    IN inYear VARCHAR(20),
    IN inRegion VARCHAR(10),
    IN inDistrict VARCHAR(2000),
    IN inTCPPort VARCHAR(10),
    IN inUDPPort VARCHAR(10),
    IN inMemo TEXT,
    IN inUserID VARCHAR(20),
    IN inUseYN INT
)
BEGIN

	START TRANSACTION;
    
		INSERT INTO `device_realtime`.`fc_master`
			(`DeviceNum`,
			`Category`,
			`Year`,
			`Region`,
			`District`,
			`TCPPort`,
			`UDPPort`,
			`InsUserID`,
			`InsDate`,
			`ModUserID`,
			`ModDate`,
            `UseYN`)
			VALUES
			(inDeviceNum,
			IFNULL(inCategory, NULL) ,
			IFNULL(inYear, NULL) ,
			IFNULL(inRegion, NULL) ,
			IFNULL(inDistrict, NULL) ,
			IFNULL(inTCPPort, NULL) ,
			IFNULL(inUDPPort, NULL) ,
			IFNULL(inUserID, NULL) ,
			NOW(),
			inUserID,
			NOW(),
            inUseYN)
			ON DUPLICATE KEY UPDATE
			`Category` = inCategory,
			`Year` = inYear,
			`Region` = inRegion,
			`District` = inDistrict,
			`TCPPort` = inTCPPort,
			`UDPPort` = inUDPPort,
			`ModUserID` = inUserID,
			`ModDate` = NOW(),
            `UseYN` = inUseYN;
			
		CALL `device_realtime`.`sp_fc_memo_common_insert`(inUserID, inDeviceNum, inMemo);

    COMMIT;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_fc_master_select`()
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    
    SELECT fm.`DeviceNum`,
		fm.`Category`,
        ccd.`CodeText` AS CategoryText,
		fm.`Year`,
		fm.`Region`,
        ccd2.`CodeText` AS RegionText,
		fm.`District`,
		fm.`TCPPort`,
		fm.`UDPPort`,
		fm.`InsUserID`,
		fm.`InsDate`,
		fm.`ModUserID`,
		fm.`ModDate`,
        fmc.`Memo` AS CommonMemo,
        fm.`UseYN`,
		CASE
			WHEN fm.`UseYN` = 1 THEN '가동중'
            ELSE '중지'
		END AS UseYNText
	FROM `fc_master` fm
		LEFT JOIN `common_code_detail` ccd
			ON ccd.DetailCode = fm.Category
		LEFT JOIN `common_code_detail` ccd2
			ON ccd2.DetailCode = fm.Region
		LEFT JOIN `fc_memo_common` fmc
			ON fmc.`DeviceNum` = fm.`DeviceNum`
    ;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_master_total_select`()
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	#실시간 생산량#
	#select 1000 as sum_total_Acp #실시간 전기 생산량 
	#	   ,200 as 	sum_total_E_Gen #열생산량
	#       ,200 as 	sum_total_H_Gen; #월 누적 생산량
	SELECT 
			  ifnull(sum(case when ms.Category = 'EC001' then 1 else 0 end ),0)  as `600wNum`
			, ifnull(sum(case when ms.Category = 'EC002' then 1 else 0 end ),0)  as `1kwNum`
            , ifnull(sum(case when ms.Category = 'EC004' then 1 else 0 end ),0)  as `5kwNum`
            , ifnull(sum(case when ms.Category = 'EC003' then 1 else 0 end ),0)  as `10kwNum`
			, ifnull(sum(case when ms.Category = 'EC002' and m.DeviceStatus ='8' and m.DataSeq > date_format(DATE_ADD(now(), INTERVAL -60 SECOND),'%Y%m%d%H%i%s') then 1 else 0 end ),0) as `1kwWait`
			, ifnull(sum(case when ms.Category = 'EC002' and m.DeviceStatus ='1' and m.DataSeq > date_format(DATE_ADD(now(), INTERVAL -60 SECOND),'%Y%m%d%H%i%s') then 1 else 0 end ),0) as `1kwIng`
			, ifnull(sum(case when ms.Category = 'EC002' and m.DeviceStatus ='10' and m.DataSeq > date_format(DATE_ADD(now(), INTERVAL -60 SECOND),'%Y%m%d%H%i%s') then 1 else 0 end ),0)  as `1kwError`
			, ifnull(sum(case when ms.Category = 'EC002' and m.DeviceStatus ='9' and m.DataSeq > date_format(DATE_ADD(now(), INTERVAL -60 SECOND),'%Y%m%d%H%i%s') then 1 else 0 end ),0)  as `1kwWaring`
            , ifnull(sum(case when ms.Category = 'EC002' then m.ACP else 0 end ),0) / 1000000  as `1kwAcp`
            ,sum(case 
				when ms.Category = 'EC002' then ifnull(frg.Total_H_gen,0) / 10
				else 0
				end
			) / 1000 as `1kwHGen`
			,sum(case 
				when ms.Category = 'EC002' then ifnull(frg.Total_E_gen,0) / 10
				else 0 
				end
			) / 1000 as `1kwEGen`
			, ifnull(sum(case when ms.Category = 'EC001' and m.DeviceStatus ='8' and m.DataSeq > date_format(DATE_ADD(now(), INTERVAL -60 SECOND),'%Y%m%d%H%i%s') then 1 else 0 end ),0) as `600wWait`
			, ifnull(sum(case when ms.Category = 'EC001' and m.DeviceStatus ='1' and m.DataSeq > date_format(DATE_ADD(now(), INTERVAL -60 SECOND),'%Y%m%d%H%i%s') then 1 else 0 end ),0) as `600wIng`
			, ifnull(sum(case when ms.Category = 'EC001' and m.DeviceStatus ='10' and m.DataSeq > date_format(DATE_ADD(now(), INTERVAL -60 SECOND),'%Y%m%d%H%i%s') then 1 else 0 end ),0)  as `600wError`
			, ifnull(sum(case when ms.Category = 'EC001' and m.DeviceStatus ='9' and m.DataSeq > date_format(DATE_ADD(now(), INTERVAL -60 SECOND),'%Y%m%d%H%i%s') then 1 else 0 end ),0)  as `600wWaring`
            ,sum(case 
				when ms.Category = 'EC001' then ifnull(frg.Total_H_gen,0) / 10 #월 누적 발전량
				else 0
				end
			) / 1000 as `600wHGen`
            , ifnull(sum(case when ms.Category = 'EC001' then m.ACP else 0 end ),0) / 1000000  as `600wAcp` #실시간 전기 생산량
			,sum(case 
				when ms.Category = 'EC001' then ifnull(frg.Total_E_gen,0) / 10 #열생산량
				else 0
				end
			) / 1000 as `600wEGen`
			, ifnull(sum(case when (ms.Category = 'EC003' or ms.Category = 'EC004') and m.DeviceStatus ='8' and m.DataSeq > date_format(DATE_ADD(now(), INTERVAL -60 SECOND),'%Y%m%d%H%i%s') then 1 else 0 end ),0) as `5_10kwWait`
			, ifnull(sum(case when (ms.Category = 'EC003' or ms.Category = 'EC004') and m.DeviceStatus ='1' and m.DataSeq > date_format(DATE_ADD(now(), INTERVAL -60 SECOND),'%Y%m%d%H%i%s') then 1 else 0 end ),0) as `5_10kwIng`
			, ifnull(sum(case when (ms.Category = 'EC003' or ms.Category = 'EC004') and m.DeviceStatus ='10' and m.DataSeq > date_format(DATE_ADD(now(), INTERVAL -60 SECOND),'%Y%m%d%H%i%s') then 1 else 0 end ),0)  as `5_10kwError`
			, ifnull(sum(case when (ms.Category = 'EC003' or ms.Category = 'EC004') and m.DeviceStatus ='9' and m.DataSeq > date_format(DATE_ADD(now(), INTERVAL -60 SECOND),'%Y%m%d%H%i%s') then 1 else 0 end ),0)  as `5_10kwWaring`
			, ifnull(sum(case when (ms.Category = 'EC003' or ms.Category = 'EC004') then m.ACP else 0 end ),0) / 1000000  as `5_10kwAcp`
			,sum(case 
				when (ms.Category = 'EC003' or ms.Category = 'EC004') then ifnull(frg.Total_H_gen,0) 
				else 0 
				end
			) / 1000 as `5_10kwHGen`
			,sum(case 
				when ms.Category = 'EC003' then ifnull(frg.Total_E_gen,0) 
				else 0 
				end
			) / 1000 as `5_10kwEGen`
            , ifnull(sum(case when m.DataSeq > (date_format(now(),'%Y%m%d%H%i%s') - 60) then 0 else 1 end ),0)  as `totalEnd`
            , '62' AS `1kwEACPRate`
            , '60' AS `1kwACPRate`
            , '0' AS `600wEACPRate`
            , '0' AS `600wACPRate`
            , '60' AS `5_10kwEACPRate`
            , '50' AS `5_10kwACPRate`
	FROM device_realtime.fc_master ms
		left join device_realtime.fc_realtime m
			on ms.DeviceNum = m.DeviceNum 
		left join device_realtime.fc_realtime_gen frg
			on frg.DeviceNum = ms.DeviceNum
	WHERE ms.UseYN = '1';

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_master_update`(
	IN inDeviceNum VARCHAR(20),
    IN inCategory VARCHAR(10),
    IN inYear VARCHAR(20),
    IN inRegion VARCHAR(10),
    IN inDistrict VARCHAR(2000),
    IN inTCPPort VARCHAR(10),
    IN inUDPPort VARCHAR(10),
    IN inMemo TEXT,
    IN inUserID VARCHAR(20)

)
BEGIN

	START TRANSACTION;

	UPDATE `device_realtime`.`fc_master`
	SET
	`Category` = inCategory,
	`Year` = IFNULL(inYear, NULL),
	`Region` = inRegion,
	`District` = IFNULL(inDistrict, NULL),
	`TCPPort` = inTCPPort,
	`UDPPort` = IFNULL(inUDPPort, NULL),
	`ModUserID` = inUserID,
	`ModDate` = NOW()
	WHERE `DeviceNum` = inDeviceNum;
    
	CALL `device_realtime`.`sp_fc_memo_common_insert`(inUserID, inDeviceNum, inMemo);

	COMMIT;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_memo_common_compare`(
	IN inDeviceNum	VARCHAR(20),
    IN inDate DATETIME,
    OUT isBool BOOL
)
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF EXISTS (SELECT 1 FROM `fc_memo_common` WHERE `DeviceNum` = inDeviceNum ) THEN
	BEGIN

		IF EXISTS (SELECT 1 FROM `fc_memo_common` WHERE `DeviceNum` = inDeviceNum AND `ModDate` = inDate ) THEN
		BEGIN
        
			SET isBool = TRUE;
            
		END;
		ELSE
		BEGIN
        
			SET isBool = FALSE;
            
		END;
		END IF;
        
	END;
    ELSE
	BEGIN
    
		SET isBool = TRUE;
        
	END;
	END IF;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_fc_memo_common_insert`(
	IN inUserID	VARCHAR(20)
    , IN inDeviceNum	VARCHAR(20)
    , IN inMemo	TEXT(2000)
)
BEGIN

	START TRANSACTION;

	INSERT INTO `fc_memo_common`
	(`DeviceNum`,
	`Memo`,
	`InsUserID`,
	`InsDate`,
	`ModUserID`,
	`ModDate`)
	VALUES
	(IFNULL(inDeviceNum, 0),
	inMemo,
	inUserID,
	NOW(),
	inUserID,
	NOW())
    ON DUPLICATE KEY UPDATE
    `Memo` = inMemo,
    `ModUserID` = inUserID,
    `ModDate` = NOW();

	COMMIT;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_memo_common_select`(
	IN inDeviceNum	VARCHAR(20)
)
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT fmc.Memo,
		fmc.DeviceNum,
		fmc.ModDate,
        CONCAT(cum.Name, '(', fmc.ModUserID, ')') AS ModeUser
	FROM fc_memo_common as fmc
		LEFT JOIN common_user_master as cum
			ON cum.UserID = fmc.ModUserID
    WHERE fmc.DeviceNum = inDeviceNum
    ;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_fc_memo_private_insert`(
	IN inUserID	VARCHAR(20)
    , IN inDeviceNum	INT
    , IN inMemo	TEXT(2000)
)
BEGIN

	START TRANSACTION;

	INSERT INTO `fc_memo_private`
	(`UserID`,
    `DeviceNum`,
	`Memo`,
	`InsDate`,
	`ModDate`)
	VALUES
	(IFNULL(inUserID, 'nulluser'),
    IFNULL(inDeviceNum, 0),
	inMemo,
	NOW(),
	NOW())
    ON DUPLICATE KEY UPDATE
    `Memo` = inMemo,
    `ModDate` = NOW();

	COMMIT;
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_memo_private_select`(
	IN inUserID	VARCHAR(20)
    , IN inDeviceNum	VARCHAR(20)
)
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT fmp.Memo,
		fmp.ModDate
	FROM fc_memo_private fmp
    WHERE fmp.UserID = inUserID
		AND fmp.DeviceNum = inDeviceNum;


END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_mylist_delete`(
	IN 	inDeviceNum VARCHAR(20),
    IN 	inUserID VARCHAR(20)
)
BEGIN

	START TRANSACTION;

	DELETE FROM `fc_mylist`
	WHERE 1=1
		AND UserID = inUserID
        AND DeviceNum = inDeviceNum;

    COMMIT;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_mylist_insert`(
	IN 	inDeviceNum VARCHAR(20),
    IN 	inUserID VARCHAR(20)
)
BEGIN

	START TRANSACTION;

	INSERT INTO `device_realtime`.`fc_mylist`
	(`DeviceNum`,
	`UserID`,
	`InsUserID`,
	`InsDate`,
	`IsFix`)
	VALUES
	(inDeviceNum,
	inUserID,
	inUserID,
	NOW(),
	0)
    ON DUPLICATE KEY UPDATE
    `InsUserID` = inUserID,
	`InsDate` = NOW(),
	`IsFix` = 0;
    
    COMMIT;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_mylist_isfix`(
	IN 	inDeviceNum VARCHAR(20),
    IN 	inUserID VARCHAR(20),
    IN 	inIsFix VARCHAR(1)
)
BEGIN

	START TRANSACTION;


	IF (inIsFix = '1') THEN
    
		UPDATE `device_realtime`.`fc_mylist`
		SET
		`IsFix` = 0
		WHERE `DeviceNum` = inDeviceNum AND `UserID` = inUserID;
        
    ELSE
    
		UPDATE `device_realtime`.`fc_mylist`
		SET
		`IsFix` = 1
		WHERE `DeviceNum` = inDeviceNum AND `UserID` = inUserID;
        
    END IF;

    COMMIT;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_mylist_left_select`(
	IN inUserID VARCHAR(20)
)
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT fm.`DeviceNum`,
		fm.`Category`,
        ccd.`CodeText` AS CategoryText,
		fm.`Year`,
		fm.`Region`,
        ccd2.`CodeText` AS RegionText,
		fm.`District`,
		fm.`TCPPort`,
		fm.`UDPPort`,
		fm.`InsUserID`,
		fm.`InsDate`,
		fm.`ModUserID`,
		fm.`ModDate`,
		fm.`UseYN`
	FROM fc_master fm
		LEFT JOIN `common_code_detail` ccd
			ON ccd.DetailCode = fm.Category
		LEFT JOIN `common_code_detail` ccd2
			ON ccd2.DetailCode = fm.Region
    WHERE 1=1
		AND fm.DeviceNum NOT IN (SELECT fml.DeviceNum FROM fc_mylist fml WHERE fml.UserID = inUserID)
		AND fm.UseYN = '1';





END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_mylist_right_select`(
	IN inUserID VARCHAR(20)
)
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT fm.`DeviceNum`,
		fm.`Category`,
        ccd.`CodeText` AS CategoryText,
		fm.`Year`,
		fm.`Region`,
        ccd2.`CodeText` AS RegionText,
		fm.`District`,
		fm.`TCPPort`,
		fm.`UDPPort`,
		fm.`InsUserID`,
		fm.`InsDate`,
		fm.`ModUserID`,
		fm.`ModDate`,
		fm.`UseYN`,
        fml.`IsFix`,
        CASE
			WHEN fml.`IsFix` = 1 THEN '관심'
            ELSE ''
		END IsFixText
	FROM fc_mylist fml
		INNER JOIN fc_master fm
			ON fm.DeviceNum = fml.DeviceNum AND fm.UseYN = '1'
		LEFT JOIN `common_code_detail` ccd
			ON ccd.DetailCode = fm.Category
		LEFT JOIN `common_code_detail` ccd2
			ON ccd2.DetailCode = fm.Region
    WHERE 1=1
		AND fml.UserID = inUserID
	ORDER BY IsFix DESC;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_mylist_select`(
	IN inLoginID	VARCHAR(20)
)
BEGIN
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT fm.DeviceNum
		, fm.Category
		, CONCAT(ccd2.CodeText, ' ', fm.District) AS District
		, ccd.CodeText AS CategoryName
		, fm.`Year`
        , frt.`Load` as xxx
        ,CASE
			WHEN frt.`Load` = '1' AND fm.Category = 'EC003' THEN '6000'
            WHEN frt.`Load` = '2' AND fm.Category = 'EC003' THEN '7500'
            WHEN frt.`Load` = '3' AND fm.Category = 'EC003' THEN '10000'
            WHEN frt.`Load` = '4' AND fm.Category = 'EC003' THEN '3500'
            WHEN frt.`Load` = '5' AND fm.Category = 'EC003' THEN '6000'
            WHEN frt.`Load` = '6' AND fm.Category = 'EC003' THEN '8500'
            WHEN frt.`Load` = '1' THEN '500'
            WHEN frt.`Load` = '2' THEN '750'
            WHEN frt.`Load` = '3' THEN '1000'
            WHEN frt.`Load` = '4' THEN '350'
            WHEN frt.`Load` = '5' THEN '600'
            WHEN frt.`Load` = '6' THEN '850'
		END AS `Load`
		, IFNULL(frt.`ACP`, 0) AS `ACP`
		,CASE 
			WHEN fm.Category = 'EC001' OR fm.Category = 'EC002' THEN ROUND(IFNULL(frg.Total_E_gen, 0) / 10, 1)
			WHEN fm.Category = 'EC003' THEN ROUND(IFNULL(frg.Total_E_gen, 0), 1)
            ELSE ROUND(IFNULL(frg.Total_E_gen, 0), 1)
		END AS Total_E_gen 
		, IFNULL(fml.IsFix, 0) AS IsFix
        , CASE 
			WHEN frt.DataSeq IS NULL THEN 0
            WHEN frt.Seq = '8200' THEN 10
            WHEN frt.Seq = '8000' THEN 0
			WHEN frt.DataSeq < (DATE_FORMAT(NOW(),'%Y%m%d%H%i%s') - 60) THEN 0
			WHEN frt.Seq = '10' OR frt.Seq = '8930' OR frt.Seq = '8900' THEN 8
            WHEN frt.Seq >= '200' AND frt.Seq <= '210' THEN 2
            WHEN frt.DeviceStatus = '10' THEN 10
            WHEN frt.DeviceStatus = '9' THEN 9
			WHEN frt.Seq >= '1000' AND frt.Seq <= '4990' THEN 1
			ELSE frt.DeviceStatus 
		END AS DeviceStatus
        /*
		, CASE 
			WHEN frt.DataSeq IS NULL THEN 0
			WHEN frt.DataSeq < (DATE_FORMAT(NOW(),'%Y%m%d%H%i%s') - 60) THEN 0
			ELSE frt.DeviceStatus 
		END AS DeviceStatus
        */
		, CASE
			WHEN frt.DataSeq IS NULL THEN '통신두절'
            WHEN frt.Seq = '8200' THEN '에러/정지'
            WHEN frt.Seq = '8000' THEN '정지'
			WHEN frt.DataSeq < (date_format(now(),'%Y%m%d%H%i%s') - 60) THEN '통신두절'
			WHEN frt.Seq = '10' OR frt.Seq = '8930' OR frt.Seq = '8900' THEN '대기'
            WHEN frt.Seq >= '200' AND frt.Seq <= '210' THEN '수동'
            WHEN frt.DeviceStatus = '10' THEN '에러'
            WHEN frt.DeviceStatus = '9' THEN '경고'
			WHEN frt.Seq >= '1000' AND frt.Seq <= '4990' THEN '가동중'
			ELSE '기타'
		END AS DeviceStatusText
        /*
        , CASE
			WHEN frt.DataSeq IS NULL THEN '두절'
			WHEN frt.DataSeq < (date_format(now(),'%Y%m%d%H%i%s') - 60) THEN '두절'
			WHEN frt.DeviceStatus = '1' THEN '정상'
			WHEN frt.DeviceStatus = '8' THEN '대기'
			WHEN frt.DeviceStatus = '10' THEN '에러'
			WHEN frt.DeviceStatus = '9' THEN '경고'
			ELSE '기타'
		END AS DeviceStatusText
        */
		, CASE
			WHEN fml.IsFix = TRUE THEN '관심'
			ELSE '일반'
		END AS IsFixText
		, CONCAT(cum.Name, '(', frc.UserID, ')') AS ConnectedUserText
		, frc.UserID AS ConnectedUserID
		, frc.Connected
        , CASE
			WHEN frc.Connected = 'Y' THEN frc.InTime
			ELSE frc.OutTime
        END AS ConnectedLastDate
        , fci.FWver
        , fci.End_day
	FROM fc_mylist fml
		INNER JOIN fc_master fm ON fm.DeviceNum = fml.DeviceNum
		LEFT JOIN fc_realtime frt ON frt.DeviceNum = fm.DeviceNum
		LEFT JOIN fc_realtime_gen frg ON frg.DeviceNum = fm.DeviceNum
		LEFT JOIN common_code_detail ccd ON ccd.DetailCode = fm.Category
		LEFT JOIN common_code_detail ccd2 ON ccd2.DetailCode = fm.Region
		LEFT JOIN fc_realtime_connected frc ON frc.DeviceNum = fm.DeviceNum
		LEFT JOIN common_user_master cum ON cum.UserID = frc.UserID
        LEFT JOIN fc_realtime_info fci ON fci.DeviceNum = fml.DeviceNum
	WHERE 1=1
		AND fml.UserID = inLoginID
	ORDER BY fml.IsFix DESC
		, DeviceStatus DESC;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_mylist_select2`(
	IN inLoginID	VARCHAR(20)
)
BEGIN
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT fm.DeviceNum
		, fm.Category
		, CONCAT(ccd2.CodeText, ' ', fm.District) AS District
		, ccd.CodeText AS CategoryName
		, fm.`Year`
		, IFNULL(frt.`Load`, 0) AS `Load`
		, IFNULL(frt.`ACP`, 0) AS `ACP`
		,CASE 
			WHEN fm.Category = 'EC001' OR fm.Category = 'EC002' THEN ROUND(IFNULL(frg.Total_E_gen, 0) / 10, 1)
			WHEN fm.Category = 'EC003' THEN ROUND(IFNULL(frg.Total_E_gen, 0), 1)
		END AS Total_E_gen 
		, IFNULL(fml.IsFix, 0) AS IsFix
		, CASE 
			WHEN frt.DataSeq IS NULL THEN 0
			WHEN frt.DataSeq < (DATE_FORMAT(NOW(),'%Y%m%d%H%i%s') - 60) THEN 0
			ELSE frt.DeviceStatus 
		END AS DeviceStatus
		, CASE
			WHEN frt.DataSeq IS NULL THEN '두절'
			WHEN frt.DataSeq < (date_format(now(),'%Y%m%d%H%i%s') - 60) THEN '두절'
			WHEN frt.DeviceStatus = '1' THEN '정상'
			WHEN frt.DeviceStatus = '8' THEN '대기'
			WHEN frt.DeviceStatus = '10' THEN '에러'
			WHEN frt.DeviceStatus = '9' THEN '경고'
			ELSE '기타'
		END AS DeviceStatusText
		, CASE
			WHEN fml.IsFix = TRUE THEN '관심'
			ELSE '일반'
		END AS IsFixText
        , '' AS ConnectedUserText
        , '' AS ConnectedUserID
        , 'N' AS Connected
        , '' AS ConnectedLastDate
        
		, CONCAT(cum.Name, '(', frc.UserID, ')') AS ConnectedUserText
		, frc.UserID AS ConnectedUserID
		, frc.Connected
        , CASE
			WHEN frc.Connected = 'Y' THEN frc.InTime
			ELSE frc.OutTime
        END AS ConnectedLastDate
        
	FROM fc_mylist fml
		INNER JOIN fc_master fm ON fm.DeviceNum = fml.DeviceNum
		LEFT JOIN fc_realtime frt ON frt.DeviceNum = fm.DeviceNum
		LEFT JOIN fc_realtime_gen frg ON frg.DeviceNum = fm.DeviceNum
		LEFT JOIN common_code_detail ccd ON ccd.DetailCode = fm.Category
		LEFT JOIN common_code_detail ccd2 ON ccd2.DetailCode = fm.Region
		LEFT JOIN fc_realtime_connected frc ON frc.DeviceNum = fm.DeviceNum
		LEFT JOIN common_user_master cum ON cum.UserID = frc.UserID
	WHERE 1=1
		AND fml.UserID = inLoginID
	ORDER BY fml.IsFix DESC
		, DeviceStatus DESC;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_realtime_gen_insert`(
	IN inDeviceNum INT
	,IN inTotal_H_Gen INT
	,IN inTotal_E_Gen INT
    ,IN inM1 INT
    ,IN inM2 INT
    ,IN inM3 INT
    ,IN inM4 INT
    ,IN inM5 INT
    ,IN inM6 INT
    ,IN inM7 INT
    ,IN inM8 INT
    ,IN inM9 INT
    ,IN inM10 INT
    ,IN inM11 INT
    ,IN inM12 INT
    ,IN inLastYear INT
    ,IN inTheYearBefore INT
)
BEGIN

	START TRANSACTION;

	INSERT INTO `device_realtime`.`fc_realtime_gen`
		(`DeviceNum`,
        `Total_H_Gen`,
		`Total_E_Gen`,
        `m1`,
		`m2`,
		`m3`,
		`m4`,
		`m5`,
		`m6`,
		`m7`,
		`m8`,
		`m9`,
		`m10`,
		`m11`,
		`m12`,
		`LastYear`,
		`TheYearBefore`)
		VALUES(inDeviceNum
        ,inTotal_H_Gen
		,inTotal_E_Gen
        ,inM1
        ,inM2
        ,inM3
        ,inM4
        ,inM5
        ,inM6
        ,inM7
        ,inM8
        ,inM9
        ,inM10
        ,inM11
        ,inM12
        ,inLastYear
        ,inTheYearBefore)
        ON DUPLICATE KEY UPDATE
      	`Total_H_Gen` = inTotal_H_Gen
		,`Total_E_Gen` = inTotal_E_Gen
        ,`m1` = inM1
        ,`m2` = inM2
        ,`m3` = inM3
        ,`m4` = inM4
        ,`m5` = inM5
        ,`m6` = inM6
        ,`m7` = inM7
        ,`m8` = inM8
        ,`m9` = inM9
        ,`m10` = inM10
        ,`m11` = inM11
        ,`m12` = inM12
        ,`LastYear` = inLastYear
        ,`TheYearBefore` = inTheYearBefore;
    
    COMMIT;
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_realtime_info_insert`(
	IN inDeviceNum INT
	,IN inFWver VARCHAR(45)
	,IN inEnd_day VARCHAR(45)
)
BEGIN

	START TRANSACTION;

	INSERT INTO `device_realtime`.`fc_realtime_info`
		(`DeviceNum`,
        `FWver`,
		`End_day`)
		VALUES(inDeviceNum
        ,inFWver
		,inEnd_day)
        ON DUPLICATE KEY UPDATE
      	`FWver` = inFWver
		,`End_day` = inEnd_day;
    
    COMMIT;
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_realtime_insert`(
	IN inDeviceNum INT
	,IN inDataSeq BIGINT
	,IN inDate VARCHAR(50)
	,IN inTime VARCHAR(50)
	,IN inPattern VARCHAR(50)
	,IN inLoad VARCHAR(50)
	,IN inACP VARCHAR(50)
	,IN inTTCogen VARCHAR(50)
	,IN inSeq VARCHAR(50)
	,IN inTT117 VARCHAR(50)
	,IN inTT102 VARCHAR(50)
	,IN inTT105 VARCHAR(50)
	,IN inTT109 VARCHAR(50)
	,IN inTT112 VARCHAR(50)
	,IN inTT123 VARCHAR(50)
	,IN inTT120 VARCHAR(50)
	,IN inTT101 VARCHAR(50)
	,IN inTT301 VARCHAR(50)
	,IN inTT302 VARCHAR(50)
	,IN inTT01 VARCHAR(50)
	,IN inTT02 VARCHAR(50)
	,IN inTT03 VARCHAR(50)
	,IN inTT04 VARCHAR(50)
	,IN inTT05 VARCHAR(50)
	,IN inTT06 VARCHAR(50)
	,IN inTT07 VARCHAR(50)
	,IN inTT08 VARCHAR(50)
	,IN inTT09 VARCHAR(50)
	,IN inTT10 VARCHAR(50)
	,IN inTT11 VARCHAR(50)
	,IN inTT12 VARCHAR(50)
	,IN inTT13 VARCHAR(50)
	,IN inTT14 VARCHAR(50)
	,IN inTT15 VARCHAR(50)
	,IN inTT16 VARCHAR(50)
	,IN inTT201 VARCHAR(50)
	,IN inTTCold VARCHAR(50)
	,IN inTTHot VARCHAR(50)
	,IN inTTA VARCHAR(50)
	,IN inPIT101 VARCHAR(50)
	,IN inPIT_NG VARCHAR(50)
	,IN inPIT201 VARCHAR(50)
	,IN inPIT301 VARCHAR(50)
	,IN inP101_SP VARCHAR(50)
	,IN inP101_MF VARCHAR(50)
	,IN inP102_SP VARCHAR(50)
	,IN inP201_SP VARCHAR(50)
	,IN inP201_MF VARCHAR(50)
	,IN inP202_SP VARCHAR(50)
	,IN inP202_MF VARCHAR(50)
	,IN inP203_SP VARCHAR(50)
	,IN inP203_MF VARCHAR(50)
	,IN inP301_SP VARCHAR(50)
	,IN inP302_SP VARCHAR(50)
	,IN inP303_SP VARCHAR(50)
	,IN inP304_SP VARCHAR(50)
	,IN inH2_Leak VARCHAR(50)
	,IN inP101_MFC_SP VARCHAR(50)
	,IN inP101_MFC_MF VARCHAR(50)
	,IN inP102_MFC_SP VARCHAR(50)
	,IN inP102_MFC_MF VARCHAR(50)
	,IN inH2_Leak_2 VARCHAR(50)
	,IN inCogen VARCHAR(50)
	,IN inP301_MF VARCHAR(50)
	,IN inStack_V VARCHAR(50)
	,IN inINV_V VARCHAR(50)
	,IN inINV_I VARCHAR(50)
	,IN inAC_V VARCHAR(50)
	,IN inStack_I VARCHAR(50)
	,IN inAC_V_A VARCHAR(50)
	,IN inAC_V_B VARCHAR(50)
	,IN inAC_V_C VARCHAR(50)
	,IN inINV_Error VARCHAR(50)
	,IN inLS303_H VARCHAR(50)
	,IN inLS303_L VARCHAR(50)
	,IN inLS304_H VARCHAR(50)
	,IN inLS304_L VARCHAR(50)
	,IN inCVM VARCHAR(50)
	,IN inNC101 VARCHAR(50)
	,IN inNC102 VARCHAR(50)
	,IN inNC103 VARCHAR(50)
	,IN inNC104 VARCHAR(50)
	,IN inNC105 VARCHAR(50)
	,IN inNC106 VARCHAR(50)
	,IN inNC202 VARCHAR(50)
	,IN inNC308 VARCHAR(50)
	,IN inNC309 VARCHAR(50)
	,IN inNC311 VARCHAR(50)
	,IN inNC312 VARCHAR(50)
	,IN inNC313 VARCHAR(50)
	,IN inNC104_AB VARCHAR(50)
	,IN inNC303 VARCHAR(50)
	,IN inNC307 VARCHAR(50)
	,IN inIgniter VARCHAR(50)
	,IN inPWR_RLY VARCHAR(50)
	,IN inACDC VARCHAR(50)
	,IN inDCDC VARCHAR(50)
	,IN inStack_RV VARCHAR(50)
	,IN inW_TT101 VARCHAR(50)
	,IN inW_TT102 VARCHAR(50)
	,IN inW_TT109 VARCHAR(50)
	,IN inW_TT117 VARCHAR(50)
	,IN inW_TT120 VARCHAR(50)
	,IN inW_TT123 VARCHAR(50)
	,IN inW_TT301 VARCHAR(50)
	,IN inW_TT302 VARCHAR(50)
	,IN inW_FAN VARCHAR(50)
	,IN inW_PITNG VARCHAR(50)
	,IN inW_PIT101 VARCHAR(50)
	,IN inW_PIT201 VARCHAR(50)
	,IN inW_PIT301 VARCHAR(50)
	,IN inW_OCV VARCHAR(50)
	,IN inW_StackV VARCHAR(50)
	,IN inW_CVM VARCHAR(50)
	,IN inW_GRID VARCHAR(50)
	,IN inW_Inverter VARCHAR(50)
	,IN inW_InvCOMM VARCHAR(50)
	,IN inW_Igniter VARCHAR(50)
	,IN inW_TempCOM VARCHAR(50)
	,IN inW_NC303 VARCHAR(50)
	,IN inW_H2Leak VARCHAR(50)
	,IN inW_STACK VARCHAR(50)
	,IN inW_P101 VARCHAR(50)
	,IN inW_P102 VARCHAR(50)
	,IN inW_P201 VARCHAR(50)
	,IN inW_P202 VARCHAR(50)
	,IN inW_P203 VARCHAR(50)
	,IN inW_P301 VARCHAR(50)
	,IN inW_P302 VARCHAR(50)
	,IN inW_TT05 VARCHAR(50)
	,IN inW_TT12 VARCHAR(50)
	,IN inW_TT13 VARCHAR(50)
	,IN inW_TT15 VARCHAR(50)
	,IN inW_TT04 VARCHAR(50)
	,IN inW_TT16 VARCHAR(50)
	,IN inW_TTA VARCHAR(50)
	,IN inW_TT14 VARCHAR(50)
	,IN inS_TT101 VARCHAR(50)
	,IN inS_TT102 VARCHAR(50)
	,IN inS_TT109 VARCHAR(50)
	,IN inS_TT117 VARCHAR(50)
	,IN inS_TT120 VARCHAR(50)
	,IN inS_TT123 VARCHAR(50)
	,IN inS_TT301 VARCHAR(50)
	,IN inS_TT302 VARCHAR(50)
	,IN inS_FAN VARCHAR(50)
	,IN inS_PITNG VARCHAR(50)
	,IN inS_PIT101 VARCHAR(50)
	,IN inS_PIT201 VARCHAR(50)
	,IN inS_PIT301 VARCHAR(50)
	,IN inS_OCV VARCHAR(50)
	,IN inS_StackV VARCHAR(50)
	,IN inS_CVM VARCHAR(50)
	,IN inS_GRID VARCHAR(50)
	,IN inS_Inverter VARCHAR(50)
	,IN inS_InvCOMM VARCHAR(50)
	,IN inS_Igniter VARCHAR(50)
	,IN inS_TempCOM VARCHAR(50)
	,IN inS_NC303 VARCHAR(50)
	,IN inS_H2Leak VARCHAR(50)
	,IN inS_STACK VARCHAR(50)
	,IN inS_P101 VARCHAR(50)
	,IN inS_P102 VARCHAR(50)
	,IN inS_P201 VARCHAR(50)
	,IN inS_P202 VARCHAR(50)
	,IN inS_P203 VARCHAR(50)
	,IN inS_P301 VARCHAR(50)
	,IN inS_P302 VARCHAR(50)
	,IN inS_TT05 VARCHAR(50)
	,IN inS_TT12 VARCHAR(50)
	,IN inS_TT13 VARCHAR(50)
	,IN inS_TT15 VARCHAR(50)
	,IN inS_TT04 VARCHAR(50)
	,IN inS_TT16 VARCHAR(50)
	,IN inS_TTA VARCHAR(50)
	,IN inS_TT14 VARCHAR(50)
	,IN inDeviceStatus VARCHAR(50)
	,IN inIsError BOOL
	,IN inIsWarning BOOL
    ,IN inProblemName VARCHAR(50)
    ,IN inProblemValue VARCHAR(50)
    
    ,IN inLANSTATE VARCHAR(50)
	,IN inConduct VARCHAR(50)
	,IN inRoomCon VARCHAR(50)
	,IN inW_Level_Con VARCHAR(50)
	,IN inW_Desulfur VARCHAR(50)
	,IN inW_CO VARCHAR(50)
	,IN inW_Conduct VARCHAR(50)
	,IN inW_Backflow VARCHAR(50)
	,IN inW_P301_F_Cnt VARCHAR(50)
	,IN inS_Level_Con VARCHAR(50)
	,IN inS_Desulfur VARCHAR(50)
	,IN inS_CO VARCHAR(50)
	,IN inS_Backflow VARCHAR(50)
	,IN inS_P301_F_Cnt VARCHAR(50)
	,IN inNC302 VARCHAR(50)
	,IN inW_P201OL VARCHAR(50)
	,IN inW_TTINT VARCHAR(50)
	,IN inW_BOPI VARCHAR(50)
	,IN inW_StackI VARCHAR(50)
	,IN inW_NGPump VARCHAR(50)
	,IN inW_TT03 VARCHAR(50)
	,IN inW_TT201 VARCHAR(50)
	,IN inW_P203OL VARCHAR(50)
	,IN inS_P201OL VARCHAR(50)
	,IN inS_TTINT VARCHAR(50)
	,IN inS_BOPI VARCHAR(50)
	,IN inS_Conduct VARCHAR(50)
	,IN inS_StackI VARCHAR(50)
	,IN inS_NGPump VARCHAR(50)
	,IN inS_TT03 VARCHAR(50)
	,IN inS_TT201 VARCHAR(50)
	,IN inS_P203OL VARCHAR(50)
    
)
BEGIN

	START TRANSACTION;

	REPLACE INTO `device_realtime`.`fc_realtime`
	(`DeviceNum`,
	`DataSeq`,
	`Date`,
	`Time`,
	`Pattern`,
	`Load`,
	`ACP`,
	`TTCogen`,
	`Seq`,
	`TT117`,
	`TT102`,
	`TT105`,
	`TT109`,
	`TT112`,
	`TT123`,
	`TT120`,
	`TT101`,
	`TT301`,
	`TT302`,
	`TT01`,
	`TT02`,
	`TT03`,
	`TT04`,
	`TT05`,
	`TT06`,
	`TT07`,
	`TT08`,
	`TT09`,
	`TT10`,
	`TT11`,
	`TT12`,
	`TT13`,
	`TT14`,
	`TT15`,
	`TT16`,
	`TT201`,
	`TTCold`,
	`TTHot`,
	`TTA`,
	`PIT101`,
	`PIT_NG`,
	`PIT201`,
	`PIT301`,
	`P101_SP`,
	`P101_MF`,
	`P102_SP`,
	`P201_SP`,
	`P201_MF`,
	`P202_SP`,
	`P202_MF`,
	`P203_SP`,
	`P203_MF`,
	`P301_SP`,
	`P302_SP`,
	`P303_SP`,
	`P304_SP`,
	`H2_Leak`,
	`P101_MFC_SP`,
	`P101_MFC_MF`,
	`P102_MFC_SP`,
	`P102_MFC_MF`,
	`H2_Leak_2`,
	`Cogen`,
	`P301_MF`,
	`Stack_V`,
	`INV_V`,
	`INV_I`,
	`AC_V`,
	`Stack_I`,
	`AC_V_A`,
	`AC_V_B`,
	`AC_V_C`,
	`INV_Error`,
	`LS303_H`,
	`LS303_L`,
	`LS304_H`,
	`LS304_L`,
	`CVM`,
	`NC101`,
	`NC102`,
	`NC103`,
	`NC104`,
	`NC105`,
	`NC106`,
	`NC202`,
	`NC308`,
	`NC309`,
	`NC311`,
	`NC312`,
	`NC313`,
	`NC104_AB`,
	`NC303`,
	`NC307`,
	`Igniter`,
	`PWR_RLY`,
	`AC/DC`,
	`DC/DC`,
	`Stack_RV`,
	`DeviceStatus`,
    `LANSTATE`,
    `Conduct`,
    `RoomCon`,
    `NC302`)
	VALUES(inDeviceNum
	,inDataSeq
	,inDate
	,inTime
	,inPattern
	,inLoad
	,inACP
	,inTTCogen
	,inSeq
	,inTT117
	,inTT102
	,inTT105
	,inTT109
	,inTT112
	,inTT123
	,inTT120
	,inTT101
	,inTT301
	,inTT302
	,inTT01
	,inTT02
	,inTT03
	,inTT04
	,inTT05
	,inTT06
	,inTT07
	,inTT08
	,inTT09
	,inTT10
	,inTT11
	,inTT12
	,inTT13
	,inTT14
	,inTT15
	,inTT16
	,inTT201
	,inTTCold
	,inTTHot
	,inTTA
	,inPIT101
	,inPIT_NG
	,inPIT201
	,inPIT301
	,inP101_SP
	,inP101_MF
	,inP102_SP
	,inP201_SP
	,inP201_MF
	,inP202_SP
	,inP202_MF
	,inP203_SP
	,inP203_MF
	,inP301_SP
	,inP302_SP
	,inP303_SP
	,inP304_SP
	,inH2_Leak
	,inP101_MFC_SP
	,inP101_MFC_MF
	,inP102_MFC_SP
	,inP102_MFC_MF
	,inH2_Leak_2
	,inCogen
	,inP301_MF
	,inStack_V
	,inINV_V
	,inINV_I
	,inAC_V
	,inStack_I
	,inAC_V_A
	,inAC_V_B
	,inAC_V_C
	,inINV_Error
	,inLS303_H
	,inLS303_L
	,inLS304_H
	,inLS304_L
	,inCVM
	,inNC101
	,inNC102
	,inNC103
	,inNC104
	,inNC105
	,inNC106
	,inNC202
	,inNC308
	,inNC309
	,inNC311
	,inNC312
	,inNC313
	,inNC104_AB
	,inNC303
	,inNC307
	,inIgniter
	,inPWR_RLY
	,inACDC
	,inDCDC
	,inStack_RV
	,inDeviceStatus
    ,inLANSTATE
    ,inConduct
    ,inRoomCon
    ,inNC302);

	/*
	INSERT INTO `device_realtime`.`fc_realtime`
	(`DeviceNum`,
	`DataSeq`,
	`Date`,
	`Time`,
	`Pattern`,
	`Load`,
	`ACP`,
	`TTCogen`,
	`Seq`,
	`TT117`,
	`TT102`,
	`TT105`,
	`TT109`,
	`TT112`,
	`TT123`,
	`TT120`,
	`TT101`,
	`TT301`,
	`TT302`,
	`TT01`,
	`TT02`,
	`TT03`,
	`TT04`,
	`TT05`,
	`TT06`,
	`TT07`,
	`TT08`,
	`TT09`,
	`TT10`,
	`TT11`,
	`TT12`,
	`TT13`,
	`TT14`,
	`TT15`,
	`TT16`,
	`TT201`,
	`TTCold`,
	`TTHot`,
	`TTA`,
	`PIT101`,
	`PIT_NG`,
	`PIT201`,
	`PIT301`,
	`P101_SP`,
	`P101_MF`,
	`P102_SP`,
	`P201_SP`,
	`P201_MF`,
	`P202_SP`,
	`P202_MF`,
	`P203_SP`,
	`P203_MF`,
	`P301_SP`,
	`P302_SP`,
	`P303_SP`,
	`P304_SP`,
	`H2_Leak`,
	`P101_MFC_SP`,
	`P101_MFC_MF`,
	`P102_MFC_SP`,
	`P102_MFC_MF`,
	`H2_Leak_2`,
	`Cogen`,
	`P301_MF`,
	`Stack_V`,
	`INV_V`,
	`INV_I`,
	`AC_V`,
	`Stack_I`,
	`AC_V_A`,
	`AC_V_B`,
	`AC_V_C`,
	`INV_Error`,
	`LS303_H`,
	`LS303_L`,
	`LS304_H`,
	`LS304_L`,
	`CVM`,
	`NC101`,
	`NC102`,
	`NC103`,
	`NC104`,
	`NC105`,
	`NC106`,
	`NC202`,
	`NC308`,
	`NC309`,
	`NC311`,
	`NC312`,
	`NC313`,
	`NC104_AB`,
	`NC303`,
	`NC307`,
	`Igniter`,
	`PWR_RLY`,
	`AC/DC`,
	`DC/DC`,
	`Stack_RV`,
	`DeviceStatus`,
    `LANSTATE`,
    `Conduct`,
    `RoomCon`,
    `NC302`)
	VALUES(inDeviceNum
	,inDataSeq
	,inDate
	,inTime
	,inPattern
	,inLoad
	,inACP
	,inTTCogen
	,inSeq
	,inTT117
	,inTT102
	,inTT105
	,inTT109
	,inTT112
	,inTT123
	,inTT120
	,inTT101
	,inTT301
	,inTT302
	,inTT01
	,inTT02
	,inTT03
	,inTT04
	,inTT05
	,inTT06
	,inTT07
	,inTT08
	,inTT09
	,inTT10
	,inTT11
	,inTT12
	,inTT13
	,inTT14
	,inTT15
	,inTT16
	,inTT201
	,inTTCold
	,inTTHot
	,inTTA
	,inPIT101
	,inPIT_NG
	,inPIT201
	,inPIT301
	,inP101_SP
	,inP101_MF
	,inP102_SP
	,inP201_SP
	,inP201_MF
	,inP202_SP
	,inP202_MF
	,inP203_SP
	,inP203_MF
	,inP301_SP
	,inP302_SP
	,inP303_SP
	,inP304_SP
	,inH2_Leak
	,inP101_MFC_SP
	,inP101_MFC_MF
	,inP102_MFC_SP
	,inP102_MFC_MF
	,inH2_Leak_2
	,inCogen
	,inP301_MF
	,inStack_V
	,inINV_V
	,inINV_I
	,inAC_V
	,inStack_I
	,inAC_V_A
	,inAC_V_B
	,inAC_V_C
	,inINV_Error
	,inLS303_H
	,inLS303_L
	,inLS304_H
	,inLS304_L
	,inCVM
	,inNC101
	,inNC102
	,inNC103
	,inNC104
	,inNC105
	,inNC106
	,inNC202
	,inNC308
	,inNC309
	,inNC311
	,inNC312
	,inNC313
	,inNC104_AB
	,inNC303
	,inNC307
	,inIgniter
	,inPWR_RLY
	,inACDC
	,inDCDC
	,inStack_RV
	,inDeviceStatus
    ,inLANSTATE
    ,inConduct
    ,inRoomCon
    ,inNC302)
    ON DUPLICATE KEY UPDATE
    `DataSeq` = inDataSeq
	,`Date` = inDate
	,`Time` = inTime
	,`Pattern` = inPattern
	,`Load` = inLoad
	,`ACP` = inACP
	,`TTCogen` = inTTCogen
	,`Seq` = inSeq
	,`TT117` = inTT117
	,`TT102` = inTT102
	,`TT105` = inTT105
	,`TT109` = inTT109
	,`TT112` = inTT112
	,`TT123` = inTT123
	,`TT120` = inTT120
	,`TT101` = inTT101
	,`TT301` = inTT301
	,`TT302` = inTT302
	,`TT01` = inTT01
	,`TT02` = inTT02
	,`TT03` = inTT03
	,`TT04` = inTT04
	,`TT05` = inTT05
	,`TT06` = inTT06
	,`TT07` = inTT07
	,`TT08` = inTT08
	,`TT09` = inTT09
	,`TT10` = inTT10
	,`TT11` = inTT11
	,`TT12` = inTT12
	,`TT13` = inTT13
	,`TT14` = inTT14
	,`TT15` = inTT15
	,`TT16` = inTT16
	,`TT201` = inTT201
	,`TTCold` = inTTCold
	,`TTHot` = inTTHot
	,`TTA` = inTTA
	,`PIT101` = inPIT101
	,`PIT_NG` = inPIT_NG
	,`PIT201` = inPIT201
	,`PIT301` = inPIT301
	,`P101_SP` = inP101_SP
	,`P101_MF` = inP101_MF
	,`P102_SP` = inP102_SP
	,`P201_SP` = inP201_SP
	,`P201_MF` = inP201_MF
	,`P202_SP` = inP202_SP
	,`P202_MF` = inP202_MF
	,`P203_SP` = inP203_SP
	,`P203_MF` = inP203_MF
	,`P301_SP` = inP301_SP
	,`P302_SP` = inP302_SP
	,`P303_SP` = inP303_SP
	,`P304_SP` = inP304_SP
	,`H2_Leak` = inH2_Leak
	,`P101_MFC_SP` = inP101_MFC_SP
	,`P101_MFC_MF` = inP101_MFC_MF
	,`P102_MFC_SP` = inP102_MFC_SP
	,`P102_MFC_MF` = inP102_MFC_MF
	,`H2_Leak_2` = inH2_Leak_2
	,`Cogen` = inCogen
	,`P301_MF` = inP301_MF
	,`Stack_V` = inStack_V
	,`INV_V` = inINV_V
	,`INV_I` = inINV_I
	,`AC_V` = inAC_V
	,`Stack_I` = inStack_I
	,`AC_V_A` = inAC_V_A
	,`AC_V_B` = inAC_V_B
	,`AC_V_C` = inAC_V_C
	,`INV_Error` = inINV_Error
	,`LS303_H` = inLS303_H
	,`LS303_L` = inLS303_L
	,`LS304_H` = inLS304_H
	,`LS304_L` = inLS304_L
	,`CVM` = inCVM
	,`NC101` = inNC101
	,`NC102` = inNC102
	,`NC103` = inNC103
	,`NC104` = inNC104
	,`NC105` = inNC105
	,`NC106` = inNC106
	,`NC202` = inNC202
	,`NC308` = inNC308
	,`NC309` = inNC309
	,`NC311` = inNC311
	,`NC312` = inNC312
	,`NC313` = inNC313
	,`NC104_AB` = inNC104_AB
	,`NC303` = inNC303
	,`NC307` = inNC307
	,`Igniter` = inIgniter
	,`PWR_RLY` = inPWR_RLY
	,`AC/DC` = inACDC
	,`DC/DC` = inDCDC
	,`Stack_RV` = inStack_RV
	,`DeviceStatus` = inDeviceStatus
    ,`LANSTATE` = inLANSTATE
    ,`Conduct` = inConduct
    ,`RoomCon` = inRoomCon
    ,`NC302` = inNC302;
    */
    
    IF inIsWarning OR inIsError THEN
    
		REPLACE INTO `device_realtime`.`fc_realtime_problem`
		(`DataSeq`,
		`DeviceNum`,
		`ProblemType`,
		`Date`,
		`Time`,
		`ProblemName`,
		`ProblemValue`)
		VALUES(inDataSeq
		,inDeviceNum
		,inDeviceStatus
		,inDate
		,inTime
		,inProblemName
		,inProblemValue);
        
		/*
		INSERT INTO `device_realtime`.`fc_realtime_problem`
		(`DataSeq`,
		`DeviceNum`,
		`ProblemType`,
		`Date`,
		`Time`,
		`ProblemName`,
		`ProblemValue`)
		VALUES(inDataSeq
		,inDeviceNum
		,inDeviceStatus
		,inDate
		,inTime
		,inProblemName
		,inProblemValue)
        ON DUPLICATE KEY UPDATE
      	`DataSeq` = inDataSeq
		,`Date` = inDate
		,`Time` = inTime
		,`ProblemValue` = inProblemValue;
        */
        
    END IF;
    
    COMMIT;
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_fc_realtime_problem_select`(
	IN inDeviceNum	BIGINT
)
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    SELECT `DataSeq`,
		`DeviceNum`,
		CASE
			WHEN `ProblemType` = '10' THEN '에러'
			WHEN `ProblemType` = '9' THEN '경고'
			WHEN `ProblemType` = '8' THEN '대기'
		ELSE '기타'
		END AS `ProblemType`,
		`Date`,
		`Time`,
		`ProblemName`,
		`ProblemValue`
	FROM `device_realtime`.`fc_realtime_problem`
	WHERE `DeviceNum` = inDeviceNum
    ORDER BY DataSeq DESC;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`%` PROCEDURE `sp_fc_realtime_select`(
	in inDeviceNum 	varchar(20)
)
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT 
	ifnull(fr.DeviceNum,0)    as DeviceNum   
	,ifnull(fr.DataSeq,0)    as DataSeq    
	,ifnull(fr.Date,0)         as 'Date'         
	,ifnull(fr.Time,0)         as 'Time'         
	,ifnull(fr.Pattern,0)      as Pattern      
	,ifnull(fr.`Load`,0)         as 'Load'         
	,ifnull(fr.ACP,0)          as ACP          
	,ifnull(fr.TTCogen,0)      as TTCogen      
	,ifnull(fr.Seq,0)          as Seq          
	,ifnull(fr.TT117,0)        as TT117        
	,ifnull(fr.TT102,0)        as TT102       
	,ifnull(fr.TT105,0)        as TT105       
	,ifnull(fr.TT109,0)        as TT109       
	,ifnull(fr.TT112,0)        as TT112        
	,ifnull(fr.TT123,0)        as TT123        
	,ifnull(fr.TT120,0)        as TT120        
	,ifnull(fr.TT101,0)        as TT101        
	,ifnull(fr.TT201,0)        as TT201        
	,ifnull(fr.TT301,0)        as TT301        
	,ifnull(fr.TT302,0)        as TT302        
	,ifnull(fr.TT01,0)         as TT01         
	,ifnull(fr.TT02,0)         as TT02         
	,ifnull(fr.TT03,0)         as TT03         
	,ifnull(fr.TT04,0)         as TT04         
	,ifnull(fr.TT05,0)         as TT05         
	,ifnull(fr.TT06,0)         as TT06         
	,ifnull(fr.TT07,0)         as TT07         
	,ifnull(fr.TT08,0)         as TT08         
	,ifnull(fr.TT09,0)         as TT09         
	,ifnull(fr.TT10,0)         as TT10         
	,ifnull(fr.TT11,0)         as TT11       
	,ifnull(fr.TT12,0)         as TT12         
	,ifnull(fr.TT13,0)         as TT13         
	,ifnull(fr.TT14,0)         as TT14         
	,ifnull(fr.TT15,0)         as TT15         
	,ifnull(fr.TT16,0)         as TT16         
	,ifnull(fr.TTCold,0)       as TTCold       
	,ifnull(fr.TTHot,0)        as TTHot       
	,ifnull(fr.TTA,0)          as TTA          
	,ifnull(fr.PIT101,0)       as PIT101       
	,ifnull(fr.PIT_NG,0)       as PIT_NG     
	,ifnull(fr.PIT201,0)       as PIT201       
	,ifnull(fr.PIT301,0)       as PIT301       
	,ifnull(fr.P101_SP,0)      as P101_SP      
	,ifnull(fr.P101_MF,0)      as P101_MF      
	,ifnull(fr.P102_SP,0)      as P102_SP     
	,ifnull(fr.P201_SP,0)      as P201_SP     
	,ifnull(fr.P201_MF,0)      as P201_MF     
	,ifnull(fr.P202_SP,0)      as P202_SP      
	,ifnull(fr.P202_MF,0)      as P202_MF      
	,ifnull(fr.P203_SP,0)      as P203_SP      
	,ifnull(fr.P203_MF,0)      as P203_MF     
	,ifnull(fr.P301_SP,0)      as P301_SP      
	,ifnull(fr.P302_SP,0)      as P302_SP     
	,ifnull(fr.P303_SP,0)      as P303_SP     
	,ifnull(fr.P304_SP,0)      as P304_SP      
	,ifnull(fr.H2_Leak,0)      as H2_Leak     
	,ifnull(fr.P101_MFC_SP,0)  as P101_MFC_SP 
	,ifnull(fr.P101_MFC_MF,0)  as P101_MFC_MF 
	,ifnull(fr.P102_MFC_SP,0)  as P102_MFC_SP 
	,ifnull(fr.P102_MFC_MF,0)  as P102_MFC_MF 
	,ifnull(fr.H2_Leak_2,0)    as H2_Leak_2   
	,ifnull(fr.Cogen,0)        as Cogen       
	,ifnull(fr.Stack_V,0)      as Stack_V     
	,ifnull(fr.INV_V,0)        as INV_V        
	,ifnull(fr.INV_I,0)        as INV_I       
	,ifnull(fr.AC_V,0)          as AC_V         
	,ifnull(fr.Stack_I,0)      as Stack_I      
	,ifnull(fr.AC_V_A,0)       as AC_V_A     
	,ifnull(fr.AC_V_B,0)       as AC_V_B     
	,ifnull(fr.AC_V_C,0)       as AC_V_C     
	,ifnull(fr.INV_Error,0)    as INV_Error    
	,ifnull(fr.LS303_H,0)    as LS303_H     
	,ifnull(fr.LS303_L,0)      as LS303_L      
	,ifnull(fr.CVM,0)        as CVM         
	,ifnull(fr.NC101,0)        as NC101        
	,ifnull(fr.NC102,0)      as NC102       
	,ifnull(fr.NC103,0)        as NC103        
	,ifnull(fr.NC104,0)      as NC104       
	,ifnull(fr.NC105,0)      as NC105       
	,ifnull(fr.NC106,0)        as NC106        
	,ifnull(fr.NC202,0)      as NC202       
	,ifnull(fr.NC308,0)        as NC308        
	,ifnull(fr.NC309,0)        as NC309        
	,ifnull(fr.NC311,0)      as NC311       
	,ifnull(fr.NC312,0)      as NC312       
	,ifnull(fr.NC313,0)        as NC313        
	,ifnull(fr.NC104_AB,0)    as NC104_AB   
	,ifnull(fr.NC303,0)      as NC303       
	,ifnull(fr.NC307,0)      as NC307       
	,ifnull(fr.Igniter,0)      as Igniter      
	,ifnull(fr.PWR_RLY,0)      as PWR_RLY      
	,ifnull(fr.`AC/DC`,0)      as ACDC       
	,ifnull(fr.`DC/DC`,0)      as DCDC       
	,ifnull(fr.Stack_RV,0)     as Stack_RV  
	,ifnull(fr.DeviceStatus,0) as DeviceStatus  
	,ifnull(fm.District,0) as District
	,ifnull(fr.P301_MF,0) as P301_MF
	,ifnull(fr.LS304_H,0) as LS304_H
    ,ifnull(fr.LS304_L,0) as LS304_L
	,ifnull(fr.RoomCon,0) as RoomCon
    ,ifnull(fr.LANSTATE,0) as LANSTATE
    ,ifnull(fr.Conduct,0) as Conduct
	FROM fc_realtime fr
		INNER JOIN fc_master fm
			ON fm.DeviceNum = fr.DeviceNum
	WHERE fr.DeviceNum= inDeviceNum;

END$$
DELIMITER ;
