﻿CREATE TABLE `common_code_detail` (
  `MasterCode` varchar(5) NOT NULL DEFAULT '',
  `DetailCode` varchar(5) NOT NULL DEFAULT '',
  `CodeText` varchar(100) NOT NULL DEFAULT '',
  `Explain` text,
  `InsDate` datetime DEFAULT NULL,
  `InsUserID` varchar(20) DEFAULT NULL,
  `ModDate` datetime DEFAULT NULL,
  `ModUserID` varchar(20) DEFAULT NULL,
  `UseYN` char(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`MasterCode`,`DetailCode`),
  KEY `idx_DetailCode` (`DetailCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `common_code_master` (
  `MasterCode` varchar(5) NOT NULL DEFAULT '',
  `Explain` text,
  `InsDate` datetime DEFAULT NULL,
  `InsUserID` varchar(20) DEFAULT NULL,
  `ModDate` datetime DEFAULT NULL,
  `ModUserID` varchar(20) DEFAULT NULL,
  `UseYN` char(1) CHARACTER SET latin1 NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`MasterCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `common_notice` (
  `Seq` int(11) NOT NULL,
  `Version` varchar(45) DEFAULT NULL,
  `Contents` longtext,
  `ModDate` datetime DEFAULT NULL,
  PRIMARY KEY (`Seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `common_user_master` (
  `UserID` varchar(20) NOT NULL DEFAULT '',
  `UserPW` varchar(45) NOT NULL DEFAULT '',
  `AuthCode` varchar(5) NOT NULL DEFAULT '',
  `Name` varchar(10) NOT NULL DEFAULT '',
  `Value1` text,
  `Value2` text,
  `Value3` text,
  `Value4` text,
  `Value5` text,
  `InsDate` datetime DEFAULT NULL,
  `InsUserID` varchar(20) DEFAULT NULL,
  `ModDate` datetime DEFAULT NULL,
  `ModUserID` varchar(20) DEFAULT NULL,
  `UseYN` char(1) CHARACTER SET latin1 NOT NULL DEFAULT '',
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `fc_acprate_10kw` (
  `Year` int(11) NOT NULL DEFAULT '2018',
  `m1` float DEFAULT '0',
  `m2` float DEFAULT '0',
  `m3` float DEFAULT '0',
  `m4` float DEFAULT '0',
  `m5` float DEFAULT '0',
  `m6` float DEFAULT '0',
  `m7` float DEFAULT '0',
  `m8` float DEFAULT '0',
  `m9` float DEFAULT '0',
  `m10` float DEFAULT '0',
  `m11` float DEFAULT '0',
  `m12` float DEFAULT '0',
  PRIMARY KEY (`Year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `fc_acprate_1kw` (
  `Year` int(11) NOT NULL DEFAULT '2018',
  `m1` float DEFAULT '0',
  `m2` float DEFAULT '0',
  `m3` float DEFAULT '0',
  `m4` float DEFAULT '0',
  `m5` float DEFAULT '0',
  `m6` float DEFAULT '0',
  `m7` float DEFAULT '0',
  `m8` float DEFAULT '0',
  `m9` float DEFAULT '0',
  `m10` float DEFAULT '0',
  `m11` float DEFAULT '0',
  `m12` float DEFAULT '0',
  PRIMARY KEY (`Year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `fc_acprate_5kw` (
  `Year` int(11) NOT NULL DEFAULT '2018',
  `m1` float DEFAULT '0',
  `m2` float DEFAULT '0',
  `m3` float DEFAULT '0',
  `m4` float DEFAULT '0',
  `m5` float DEFAULT '0',
  `m6` float DEFAULT '0',
  `m7` float DEFAULT '0',
  `m8` float DEFAULT '0',
  `m9` float DEFAULT '0',
  `m10` float DEFAULT '0',
  `m11` float DEFAULT '0',
  `m12` float DEFAULT '0',
  PRIMARY KEY (`Year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `fc_master` (
  `DeviceNum` varchar(40) NOT NULL,
  `Category` varchar(5) DEFAULT NULL COMMENT '구분\n',
  `Year` varchar(20) DEFAULT NULL COMMENT '년식\\n',
  `Region` varchar(5) DEFAULT NULL COMMENT '지역\n',
  `District` varchar(1000) DEFAULT NULL COMMENT '세부지역\\n',
  `TCPPort` varchar(10) DEFAULT NULL COMMENT '포',
  `UDPPort` varchar(10) DEFAULT NULL,
  `InsUserID` varchar(20) DEFAULT NULL,
  `InsDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `ModUserID` varchar(20) DEFAULT NULL,
  `ModDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `UseYN` int(11) DEFAULT '1',
  PRIMARY KEY (`DeviceNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `fc_memo_common` (
  `DeviceNum` varchar(20) NOT NULL,
  `Memo` text,
  `InsUserID` varchar(20) DEFAULT NULL,
  `InsDate` datetime DEFAULT NULL,
  `ModUserID` varchar(20) DEFAULT NULL,
  `ModDate` datetime DEFAULT NULL,
  PRIMARY KEY (`DeviceNum`),
  KEY `idx_DeviceNum_ModDate` (`DeviceNum`,`ModDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `fc_memo_private` (
  `UserID` varchar(20) NOT NULL,
  `DeviceNum` int(11) NOT NULL,
  `Memo` text,
  `InsDate` datetime DEFAULT NULL,
  `ModDate` datetime DEFAULT NULL,
  PRIMARY KEY (`UserID`,`DeviceNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `fc_mylist` (
  `DeviceNum` varchar(45) NOT NULL COMMENT '장비번호',
  `UserID` varchar(20) NOT NULL COMMENT '사용자아이디',
  `InsUserID` varchar(20) DEFAULT NULL COMMENT '등록자',
  `InsDate` datetime DEFAULT NULL COMMENT '등록일',
  `IsFix` varchar(10) DEFAULT '0',
  PRIMARY KEY (`DeviceNum`,`UserID`),
  KEY `idx_UserID` (`UserID`),
  KEY `idx_DeviceNum` (`DeviceNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `fc_realtime` (
  `DeviceNum` int(11) NOT NULL COMMENT '장비번호',
  `DataSeq` bigint(20) NOT NULL COMMENT '년월일시분초',
  `Date` date DEFAULT NULL COMMENT '년월일	0x0129~C\\r\\n\\r\\n',
  `Time` time DEFAULT NULL COMMENT '시스템 시간	0x012D~F\\r\\n\\r\\n',
  `Pattern` float DEFAULT '0' COMMENT '패턴	0x0045\\\\r\\\\n',
  `Load` float DEFAULT '0' COMMENT '발전 모드	0x0044\\\\r\\\\n',
  `ACP` float DEFAULT '0' COMMENT '실시간 발전량	0x00FE\\\\r\\\\n',
  `TTCogen` float DEFAULT '0' COMMENT '온수 온도	0x0178\\\\r\\\\n',
  `Seq` float DEFAULT '0' COMMENT '시퀀스 번호	0x006E\\\\r\\\\n',
  `TT117` float DEFAULT '0' COMMENT 'Reformer 입구	0x00DA\\r\\n',
  `TT102` float DEFAULT '0' COMMENT 'Reformer	0x00D2\\\\r\\\\n',
  `TT105` float DEFAULT '0',
  `TT109` float DEFAULT '0' COMMENT 'CO-shift 상	0x00D6\\\\r\\\\n',
  `TT112` float DEFAULT '0' COMMENT 'CO-shift 하	0x00D8\\\\r\\\\n',
  `TT123` float DEFAULT '0' COMMENT 'Prox 중	0x00DE\\\\r\\\\n',
  `TT120` float DEFAULT '0' COMMENT 'Burner 온도	0x00DC\\\\r\\\\n',
  `TT101` float DEFAULT '0' COMMENT 'steam	0x00D0\\\\r\\\\n',
  `TT201` float DEFAULT '0' COMMENT '스택 공기 ',
  `TT301` float DEFAULT '0' COMMENT '스택 냉각수 입구	0x00E6\\\\r\\\\n',
  `TT302` float DEFAULT '0' COMMENT '스택 냉각수 출구	0x00E8\\\\r\\\\n',
  `TT01` float DEFAULT '0' COMMENT 'Reformer2	0x00D0\\\\r\\\\n',
  `TT02` float DEFAULT '0' COMMENT 'Reformer 입구	0x00D2\\\\r\\\\n',
  `TT03` float DEFAULT '0' COMMENT 'CO-shift 입구	0x00D4\\\\r\\\\n',
  `TT04` float DEFAULT '0' COMMENT 'Reformer	0x00D6\\\\r\\\\n',
  `TT05` float DEFAULT '0' COMMENT 'CO-shift 상	0x00D8\\\\r\\\\n',
  `TT06` float DEFAULT '0' COMMENT 'CO-shift 하	0x00DA\\\\r\\\\n',
  `TT07` float DEFAULT '0' COMMENT '0x00DC\\\\r\\\\n',
  `TT08` float DEFAULT '0' COMMENT 'CO-shift heater 상	0x00DE\\\\r\\\\n',
  `TT09` float DEFAULT '0' COMMENT 'CO-shift heater 하	0x01A4\\\\r\\\\n',
  `TT10` float DEFAULT '0' COMMENT '0x01A6\\\\r\\\\n',
  `TT11` float DEFAULT '0' COMMENT 'Prox Heater	0x01A8\\\\r\\\\n',
  `TT12` float DEFAULT '0' COMMENT 'Prox 상	0x01AA\\\\r\\\\n',
  `TT13` float DEFAULT '0' COMMENT 'Prox 중	0x01AC\\\\r\\\\n',
  `TT14` float DEFAULT '0' COMMENT 'Prox 하 (연도별 다름)	0x01AE\\\\r\\\\n',
  `TT15` float DEFAULT '0' COMMENT 'Burner 온도	0x01B0\\\\r\\\\n',
  `TT16` float DEFAULT '0' COMMENT 'steam	0x01B2\\\\r\\\\n',
  `TTCold` float DEFAULT '0' COMMENT '열 회수 후	0x0166\\\\r\\\\n',
  `TTHot` float DEFAULT '0' COMMENT '열 회수 전	0x0168\\\\r\\\\n',
  `TTA` float DEFAULT '0' COMMENT '시스템 내부온도	0x00EC\\\\r\\\\n',
  `PIT101` float DEFAULT '0' COMMENT 'Process NG 공급	0x0074\\\\r\\\\n',
  `PIT_NG` float DEFAULT '0' COMMENT '스택 공급 압력	0x0076\\\\r\\\\n',
  `PIT201` float DEFAULT '0' COMMENT '정량수 라인 압력	0x0078\\\\r\\\\n',
  `PIT301` float DEFAULT '0' COMMENT '스택 냉각수 압력	0x007A\\\\r\\\\n',
  `P101_SP` float DEFAULT '0' COMMENT 'Process NG	0x0010\\\\r\\\\n',
  `P101_MF` float DEFAULT '0' COMMENT 'Process NG	0x0062\\\\r\\\\n',
  `P102_SP` float DEFAULT '0' COMMENT '버너 NG	0x0010\\\\r\\\\n',
  `P201_SP` float DEFAULT '0' COMMENT '버너 공기	0x0012\\\\r\\\\n',
  `P201_MF` float DEFAULT '0' COMMENT '버너 공기	0x0064\\\\r\\\\n',
  `P202_SP` float DEFAULT '0' COMMENT 'Prox Air	0x0014\\\\r\\\\n',
  `P202_MF` float DEFAULT '0' COMMENT 'Prox Air	0x0066\\\\r\\\\n',
  `P203_SP` float DEFAULT '0' COMMENT '스택 공기	0x0016\\\\r\\\\n',
  `P203_MF` float DEFAULT '0' COMMENT '스택 공기	0x0068\\\\r\\\\n',
  `P301_SP` float DEFAULT '0' COMMENT '정량펌프	0x001A\\\\r\\\\n\\\\r\\\\n',
  `P302_SP` float DEFAULT '0' COMMENT '스택냉각수	0x001C\\\\r\\\\n',
  `P303_SP` float DEFAULT '0' COMMENT '폐열회수1	0x0160\\\\r\\\\n',
  `P304_SP` float DEFAULT '0' COMMENT '폐열회수2	0x0162\\\\r\\\\n',
  `H2_Leak` float DEFAULT '0' COMMENT '발전부 가연성 센서	0x006C\\\\r\\\\n',
  `P101_MFC_SP` float DEFAULT '0' COMMENT 'Process NG	0x01BA\\\\r\\\\n',
  `P101_MFC_MF` float DEFAULT '0' COMMENT 'Process NG	0x019C\\\\r\\\\n',
  `P102_MFC_SP` float DEFAULT '0' COMMENT '버너 NG	0x01CE\\\\r\\\\n',
  `P102_MFC_MF` float DEFAULT '0' COMMENT '버너 NG	0x006A\\\\r\\\\n',
  `H2_Leak_2` float DEFAULT '0' COMMENT '개질기부 가연성 센서	0x01D4\\\\r\\\\n',
  `Cogen` float DEFAULT '0' COMMENT 'Cogen 유량	0x0164\\\\r\\\\n',
  `P301_MF` float DEFAULT '0',
  `Stack_V` float DEFAULT '0' COMMENT '스택 출력	0x007E\\\\r\\\\n',
  `INV_V` float DEFAULT '0' COMMENT '인버터 정보	0x00F0\\\\r\\\\n',
  `INV_I` float DEFAULT '0' COMMENT '인버터 정보	0x00F2\\\\r\\\\n',
  `AC_V` float DEFAULT '0' COMMENT '인버터 정보	0x00FA\\\\r\\\\n',
  `Stack_I` float DEFAULT '0' COMMENT '스택 출력	0x0094\\\\r\\\\n',
  `AC_V_A` float DEFAULT '0' COMMENT '인버터 정보	0x00F6\\\\r\\\\n',
  `AC_V_B` float DEFAULT '0' COMMENT '인버터 정보	0x00F8\\\\r\\\\n',
  `AC_V_C` float DEFAULT '0' COMMENT '인버터 정보	0x00FA\\\\r\\\\n',
  `INV_Error` float DEFAULT '0' COMMENT '0x010A\\\\r\\\\n',
  `LS303_H` float DEFAULT '0' COMMENT '정량수 탱크 수위	0x0126\\\\r\\\\n',
  `LS303_L` float DEFAULT '0' COMMENT '정량수 탱크 수위	0x0126\\\\r\\\\n',
  `LS304_H` float DEFAULT '0',
  `LS304_L` float DEFAULT '0',
  `CVM` float DEFAULT '0' COMMENT 'CVM	0x0126\\\\r\\\\n',
  `NC101` float DEFAULT '0' COMMENT 'PROCESS NG 가스공급	0x0120\\\\r\\\\n',
  `NC102` float DEFAULT '0' COMMENT 'LNG 공급밸브	0x0120\\\\r\\\\n',
  `NC103` float DEFAULT '0' COMMENT '버너 NG 공급	0x0120\\\\r\\\\n',
  `NC104` float DEFAULT '0' COMMENT '스택 H2 공급	0x0120\\\\r\\\\n',
  `NC105` float DEFAULT '0' COMMENT 'OFFGAS 밸브	0x0120\\\\r\\\\n',
  `NC106` float DEFAULT '0' COMMENT 'H2 개질기 바이패스	0x0120\\\\r\\\\n',
  `NC202` float DEFAULT '0' COMMENT 'PROX 공급 밸브	0x0120\\\\r\\\\n',
  `NC308` float DEFAULT '0' COMMENT '개질기 드레인 밸브	0x0121\\\\r\\\\n',
  `NC309` float DEFAULT '0' COMMENT 'Tank1 Drain	0x0121\\\\r\\\\n',
  `NC311` float DEFAULT '0' COMMENT '0x0121\\\\r\\\\n',
  `NC312` float DEFAULT '0' COMMENT '축열조 Drain	0x0123\\\\r\\\\n',
  `NC313` float DEFAULT '0' COMMENT '기포제거 밸브	0x0122\\\\r\\\\n',
  `NC104_AB` float DEFAULT '0' COMMENT '스택 H2 공급	0x0120\\\\r\\\\n',
  `NC303` float DEFAULT '0' COMMENT 'Tank1급수	0x0121\\\\r\\\\n',
  `NC307` float DEFAULT '0' COMMENT 'Tank2급수	0x0121\\\\r\\\\n',
  `Igniter` float DEFAULT '0' COMMENT '이그니터	0x0122\\\\r\\\\n',
  `PWR_RLY` float DEFAULT '0' COMMENT '파워릴레이	0x0122\\\\r\\\\n',
  `AC/DC` float DEFAULT '0' COMMENT 'ACDC	0x0123\\\\r\\\\n',
  `DC/DC` float DEFAULT '0' COMMENT 'DCDC	0x0123\\\\r\\\\n',
  `Stack_RV` float DEFAULT '0' COMMENT '0x0122\\\\r\\\\n',
  `DeviceStatus` float DEFAULT '0',
  `LANSTATE` float DEFAULT '0',
  `Conduct` float DEFAULT '0',
  `RoomCon` float DEFAULT '0',
  `NC302` float DEFAULT '0',
  PRIMARY KEY (`DeviceNum`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `fc_realtime_connected` (
  `UserID` varchar(20) DEFAULT NULL,
  `DeviceNum` int(11) NOT NULL,
  `InTime` datetime DEFAULT NULL,
  `OutTime` datetime DEFAULT NULL,
  `Connected` char(1) DEFAULT NULL,
  PRIMARY KEY (`DeviceNum`),
  KEY `idx_DeviceNum` (`DeviceNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `fc_realtime_gen` (
  `DeviceNum` int(11) NOT NULL,
  `Total_H_Gen` int(11) DEFAULT '0',
  `Total_E_Gen` int(11) DEFAULT '0',
  `m1` int(11) DEFAULT '0',
  `m2` int(11) DEFAULT '0',
  `m3` int(11) DEFAULT '0',
  `m4` int(11) DEFAULT '0',
  `m5` int(11) DEFAULT '0',
  `m6` int(11) DEFAULT '0',
  `m7` int(11) DEFAULT '0',
  `m8` int(11) DEFAULT '0',
  `m9` int(11) DEFAULT '0',
  `m10` int(11) DEFAULT '0',
  `m11` int(11) DEFAULT '0',
  `m12` int(11) DEFAULT '0',
  `LastYear` int(11) DEFAULT '0',
  `TheYearBefore` int(11) DEFAULT '0',
  PRIMARY KEY (`DeviceNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `fc_realtime_info` (
  `DeviceNum` int(11) NOT NULL,
  `FWver` varchar(45) DEFAULT NULL,
  `End_day` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`DeviceNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `fc_realtime_problem` (
  `DataSeq` bigint(20) NOT NULL,
  `DeviceNum` int(11) NOT NULL,
  `ProblemType` int(11) NOT NULL,
  `Date` varchar(45) NOT NULL,
  `Time` varchar(45) NOT NULL,
  `ProblemName` varchar(45) NOT NULL,
  `ProblemValue` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`DeviceNum`,`ProblemType`,`ProblemName`),
  KEY `idx_devicenum` (`DeviceNum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
