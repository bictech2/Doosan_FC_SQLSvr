﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Diagnostics;
using System.IO;
//using MySql.Data.MySqlClient;
using System.Runtime.CompilerServices;
using System.Data.OracleClient;
using System.Configuration;
using Oracle.ManagedDataAccess;

namespace SQL_Svr
{
    public partial class Form1 : Form
    {
        Thread MainListenThread;                                  //메인 수신대기 스레드
        Thread DataListenThread;                                  //메인 수신대기 스레드
        //2018.07.03
        Thread WatchDogThread;                                  //Watchdog 스레드
        public UInt16 OpenPort_Number;
        public UInt16 DeviceOpenPort_Number;
        public UInt16[] DevicePort_Number;
        public UInt16 ToOpenDevicePort;
        public bool Flag_DeviceConnect;
        //2016.6.6 ok Modified - connections 전역 변수로 추가
        public UInt16 connections;

        //2017.12.15----------------------------------------------------------------------------------
        public int[] WorkingThread_ID;
        public int[] WorkingThread_Port;
        public UInt16 ThreadCnt;
        public bool Flag_ClosingThread = false;
        public int ClosingThread_Port;
        public bool Flag_ListenThreadAlive = true;

        //2018.07.03----------------------------------------------------------------------------------
        public UInt16[] AddOpenPort_Array;
        public bool Flag_ProgramStop;
        public string UserID;


        public string Matrix_Decrypt(int[,] matrixB)
        {

            Encoding encoding = Encoding.ASCII;
            var bb = 0x0d;
            int[,] matrix;

            matrix = new int[3, 3];

            matrix[0, 0] = 0;
            matrix[0, 1] = 1;
            matrix[0, 2] = -1;

            matrix[1, 0] = 0;
            matrix[1, 1] = -1;
            matrix[1, 2] = 2;

            matrix[2, 0] = 1;
            matrix[2, 1] = -1;
            matrix[2, 2] = 1;

            String encode_str = "";

            int zz = matrixB.GetLength(0);

            for (int j = 0; j < matrixB.GetLength(0); j++)
            {
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    bb = matrixB[j, 0] * matrix[0, i] + matrixB[j, 1] * matrix[1, i] + matrixB[j, 2] * matrix[2, i];
                    byte[] bt = new byte[1];
                    bt[0] = Convert.ToByte(bb);

                    encode_str = encode_str + encoding.GetString(bt);
                }
            }
            return encode_str;
        }

        public string Matrix_Make(byte[] indata)
        {
            //            string hexValues = "01B1 00DE 0064 01A3 00CF 0073 00C8 0065 0031 0116 0084 0034 002A 0000 002A";
            //            string[] hexValuesSplit = hexValues.Split(' ');



            // password array의 총 count 
            int arr_length = indata.GetLength(0);
            int kk = 0;

            // 행렬의 행 수를 계산
            int arr_col = arr_length / 3;

            //HEX로 들어온 패스워드 array를 dec로 변환하여 행렬에 저장
            int[,] matrixB;
            matrixB = new int[arr_col, 3];

            for (int i = 0; i < arr_col; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    matrixB[i, j] = Convert.ToInt32(indata[kk].ToString(), 16);
                    kk++;
                }
            }

            // 행렬의 복호화 실행
            string Decryp_str = Matrix_Decrypt(matrixB);
            // NULL 값을 없애줌
            Decryp_str = Decryp_str.Replace("\0", "");

            return Decryp_str;
        }

        


        public Form1()
        {
            InitializeComponent();
            MainListenThread = new Thread(new ParameterizedThreadStart(ServerStart));
            MainListenThread.Start();
            MainListenThread.IsBackground = true;

            DataListenThread = new Thread(new ParameterizedThreadStart(DataListenStart));
            DataListenThread.Start();
            DataListenThread.IsBackground = true;

            CheckForIllegalCrossThreadCalls = false;        //Debug 모드에서 크로스 스레드 작업 오류 제거
            ServerIP.Text = getMyIp();
            OpenPort_Number = 4000;        //Port 4000 번지 부터 사용 여부 확인
            DeviceOpenPort_Number = 63000;
            DevicePort_Number = new UInt16[100];
            //2016.6.6 ok Modified - connections 초기값 설정
            connections = 0;

            //2017.12.15----------------------------------------------------------------------------------
            ThreadPool.SetMaxThreads(100, 100);
            WorkingThread_ID = new int[100];
            WorkingThread_Port = new int[100];
            ThreadCnt = 0;

            //2018.07.03-------------------------------------------------------------------------
            WatchDogThread = new Thread(new ParameterizedThreadStart(WatchDogStart));
            WatchDogThread.Start();
            WatchDogThread.IsBackground = true;

            AddOpenPort_Array = new UInt16[20];
            Flag_ProgramStop = false;



//            // test start


//            //string hexValues = "01B1 00DE 0064 01A3 00CF 0073 00C8 0065 0031 0116 0084 0034 002A 0000 002A";
//            //string[] hexValuesSplit = hexValues.Split(' ');

//            //// password array의 총 count 
//            //int arr_length = hexValuesSplit.GetLength(0);
//            //// password array의  index
//            //int kk = 0;

//            //// 행렬의 행 수를 계산
//            //int arr_col = arr_length / 3;

//            ////HEX로 들어온 패스워드 array를 dec로 변환하여 행렬에 저장
//            //int[,] matrixB;
//            //matrixB = new int[arr_col, 3];

//            //for (int i = 0; i < arr_col; i++)
//            //{
//            //    for (int j = 0; j < 3; j++)
//            //    {
//            //        matrixB[i, j] = Convert.ToInt32(hexValuesSplit[kk],16) ;
//            //        kk++;
//            //    }
//            //}
            
//            //// 행렬의 복호화 실행
//            //string Decryp_str = Matrix_Decrypt(matrixB);
//            //// NULL 값을 없애줌
//            //Decryp_str = Decryp_str.Replace("\0", "");

//            //MessageBox.Show(Decryp_str);


//            Encoding encoding = Encoding.UTF8;
////            string str = "admin\rdfadmin!@1\r";
//            string str = "01B1 00DE 0064 01A3 00CF 0073 00C8 0065 0031 0116 0084 0034 002A 0000 002A";
//            byte[] data = new byte[1];

//            string hexString = "01B100DE006401A00CF007300C800650031011600840034002A0000002A";
//            byte[] xbytes = new byte[hexString.Length / 2];
//            string[] strarr = new string[15];
//            for (int i = 0; i < xbytes.Length; i++)
//            {
//                strarr[i] = hexString.Substring(i * 2, 2);

//                xbytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
//            }

//            string encoded1 = Encoding.Default.GetString(data);
//            MessageBox.Show(encoded1);


//            int Password_StartCnt = 0;
//            int Device_StartCnt = 0;
//            byte[] ID, PW;
//            string IDstr = "";
//            string PWstr = "";

//            for (int i = 0; i < data.Length; i++)
//            {
//                Password_StartCnt++;
//                if (data[i] == 0x0d) //첫번째 CR ... 아이디
//                {
//                    ID = new byte[i];
//                    for (int k = 0; k < ID.Length; k++)
//                    {
//                        ID[k] = data[k];
//                    }

//                    // id의 복호화 실행
//                    IDstr = Matrix_Make(ID);
//                    // NULL 값을 없애줌
//                    IDstr = IDstr.Replace("\0", "");

//                    ////main.UpdateListView(BitConverter.ToString(data));
//                    //IDstr = Encoding.Default.GetString(ID);
//                    //IDstr = IDstr.Replace("\0", string.Empty);  //  \0 : null
//                    break;
//                }
//            }
//            for (int i = Password_StartCnt; i < data.Length; i++)
//            {
//                Device_StartCnt++;
//                if (data[i] == 0x0d) //두번째 CR ... 패스워드
//                {
//                    PW = new byte[i - Password_StartCnt];
//                    for (int k = 0; k < PW.Length; k++)
//                    {
//                        PW[k] = data[k + Password_StartCnt];
//                    }
//                    PWstr = Encoding.Default.GetString(PW);
//                    PWstr = PWstr.Replace("\0", string.Empty);
//                    break;
//                }
//            }

//            MessageBox.Show("IDstr:" + IDstr + "\n" + "PWstr :" + PWstr);

//            // test end



            // ***********************  Hill Cipher Test **************************/


            /*
            string uid = "inhyeok.jung";

            OracleConnection conn = new OracleConnection(ConfigurationManager.AppSettings["strConn"]);
            //            string sql = @"SELECT * FROM common_user_master where UserID = '" + uid + "' and UserPW =pkg_encrypt_decrypt.encrypt('" + uid + "')";
            string sql = @"SELECT * FROM common_user_master where UserID = '" + uid + @"' and UserPW =pkg_encrypt_decrypt.encrypt('" + uid + @"')";
            DataSet ds = new DataSet();
            OracleDataAdapter adpt = new OracleDataAdapter(sql, conn);

            adpt.Fill(ds, "common_user_master");
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    UpdateListView("ID : " + uid + ", Password : " + uid);
                }
                else
                {
                    UpdateListView("ID : " + uid + ", Password : " + uid);
                    UpdateListView("Access Denied. Check ID and Password.");
                    conn.Close();
                    adpt.Dispose();
                    ds.Dispose();
                    //    return 0;
                }
            }
            else
            {
                UpdateListView("일치하는 Database가 없습니다.");
                conn.Close();
                adpt.Dispose();
                ds.Dispose();
                //   return 0;
            }
            conn.Close();
            adpt.Dispose();
            ds.Dispose();

            string DeviceStr = "11818";
            var value = 0;

            ////장비의 TCP Port를 DB에서 읽어옴
#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다.
            OracleConnection conn2 = new OracleConnection(ConfigurationManager.AppSettings["strConn"]);
#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.
            DataSet ds2 = new DataSet();
            string sql2 = @"SELECT TCPPort FROM fc_master where DeviceNum = '" + DeviceStr + @"' ";
#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다.
            OracleDataAdapter adpt2 = new OracleDataAdapter(sql2, conn2);
#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.

            adpt2.Fill(ds2, "fc_master");
            if (ds2.Tables.Count > 0)
            {
                if (ds2.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow r2 in ds2.Tables[0].Rows)
                    {
                        value = Convert.ToUInt16(r2["TCPPort"]);
                    }
                    UpdateListView("DeviceNum : " + DeviceStr + ". TCP Port : " + value);
                }
                else
                {
                    UpdateListView(DeviceStr + " 디바이스를 찾을 수 없습니다.");
                    conn2.Close();
                    adpt2.Dispose();
                    ds2.Dispose();
                    //return 4;
                }
            }
            else
            {
                UpdateListView("NO Table Error");
                conn2.Close();
                adpt2.Dispose();
                ds2.Dispose();
                //return 4;
            }
            conn2.Close();
            adpt2.Dispose();
            ds2.Dispose();

#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다.
            OracleCommand cmd3 = new OracleCommand("SP_FC_CONNECTED_YES", new OracleConnection(ConfigurationManager.AppSettings["strConn"]));
#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.
            cmd3.CommandType = CommandType.StoredProcedure;

            cmd3.Parameters.Add(new OracleParameter("iv_UserId", uid));
            cmd3.Parameters.Add(new OracleParameter("iv_DeviceNum", DeviceStr ));

            cmd3.Connection.Open();
            cmd3.ExecuteNonQuery();

            UpdateListView(cmd3.Connection.State.ToString()  );

            cmd3.Connection.Close();


#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다.
            OracleCommand cmd4 = new OracleCommand("SP_FC_CONNECTED_NO", new OracleConnection(ConfigurationManager.AppSettings["strConn"]));
#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.
            cmd4.CommandType = CommandType.StoredProcedure;

            cmd4.Parameters.Add(new OracleParameter("iv_UserId", uid));
            cmd4.Parameters.Add(new OracleParameter("iv_DeviceNum", DeviceStr));
            cmd4.Parameters.Add(new OracleParameter("iv_Status", "1"));

            cmd4.Connection.Open();
            cmd4.ExecuteNonQuery();
            UpdateListView(cmd4.Connection.State.ToString());
            cmd4.Connection.Close();
*/
            //#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다.

            //            OracleCommand cmd3 = new OracleCommand("SP_FC_CONNECTED_NO", new OracleConnection(ConfigurationManager.AppSettings["strConn"]));

            //#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.
            //            cmd3.CommandType = CommandType.StoredProcedure;

            //            cmd3.Parameters.Add(new OracleParameter("iv_UserId", "admin"));
            //            cmd3.Parameters.Add(new OracleParameter("iv_DeviceNum", "11111"));
            //            cmd3.Parameters.Add(new OracleParameter("iv_Status", "1"));

            //            cmd3.Connection.Open();
            //            cmd3.ExecuteNonQuery();
            //            cmd3.Connection.Close();

            //            ////장비의 TCP Port를 DB에서 읽어옴
            //#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다.
            //            OracleConnection conn2 = new OracleConnection(ConfigurationManager.AppSettings["strConn"]);
            //#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.
            //            DataSet ds2 = new DataSet();
            //            string sql2 = "SELECT TCPPort FROM fc_master where DeviceNum = '11818' ";
            //#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다.
            //            OracleDataAdapter adpt2 = new OracleDataAdapter(sql2, conn2);
            //#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.




        }
        public void ServerStart(object obj)
        {
            TcpListener Main_listener;
            RuntimeHelpers.PrepareConstrainedRegions();
            try
            {
                Main_listener = new TcpListener(IPAddress.Any, 64000);
                Main_listener.Start();
                while (true)
                {
                    while (!Main_listener.Pending())
                    {
                        //2018.07.10
                        if (Flag_ProgramStop)
                        {
                            UpdateListView("Program Stop Signal Coming.");
                            Application.ExitThread();
                            Environment.Exit(0);
                        }
                        Thread.Sleep(1000);
                    }
                    ClientConnection newconnection = new ClientConnection(this);
                    newconnection.ConnectClient = Main_listener;
                    ThreadPool.QueueUserWorkItem(new WaitCallback(newconnection.SeperateConnection));
                    Thread.Sleep(1000);         //통신 연결을 위한 대기 시간 설정
                }



            }
            catch (Exception e)
            {
                UpdateListView(e.ToString());
                ErrorLoging(e.Message, e.StackTrace);
            }
            //2018.09.03 ok 추가
            finally
            {
                UpdateListView("Main Listener Thread 종료.");
                Thread.Sleep(3000);
            }
        }                                       //Main 접속대기 스레드 시작
        public void DataListenStart(object obj)
        {
            textBox5.Text = "True";
            TcpListener Data_listener = new TcpListener(IPAddress.Any, OpenPort_Number);
            bool Flag_ConnectionSuccess = false;
            RuntimeHelpers.PrepareConstrainedRegions();
            try
            {
                while (true)
                {
                    //2018.07.03
                    //동시 접속시 Queue 순차적으로 포트 Open
                    //for (int i = 0; i < 20; i++)
                    //{
                    //    if (AddOpenPort_Array[i] > 0)
                    //    {
                    //        Flag_DeviceConnect = true;
                    //    }
                    //    else
                    //    {
                    //        Flag_DeviceConnect = false;
                    //    }
                    //}
                    if (Flag_DeviceConnect == true)
                    {
                        short ListenWaitTime = 0;
                        Flag_DeviceConnect = false;
                        //2018.07.03---------------------------------------
                        //ToOpenDevicePort = AddOpenPort_Array[0];
                        //for (int i = 0; i < 20; i++)
                        //{
                        //    if (i != 19)
                        //    {
                        //        if (AddOpenPort_Array[i] == 0)
                        //        {
                        //            AddOpenPort_Array[i] = AddOpenPort_Array[i + 1];
                        //            AddOpenPort_Array[i + 1] = 0;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        AddOpenPort_Array[18] = AddOpenPort_Array[19];
                        //        AddOpenPort_Array[19] = 0;
                        //    }
                        //}
                        //2017.12.19---------------------------------------
                        if (!ScanPortUse(ToOpenDevicePort))
                        {
                            RuntimeHelpers.PrepareConstrainedRegions();
                            try
                            {
                                Data_listener = new TcpListener(IPAddress.Any, ToOpenDevicePort);
                                Data_listener.Start();
                                UpdateListView(ToOpenDevicePort + " Port Listen start");

                                while (!Data_listener.Pending())
                                {
                                    Flag_ConnectionSuccess = true;
                                    if (ListenWaitTime == 10)
                                    {
                                        Flag_ConnectionSuccess = false;
                                        break;
                                    }
                                    UpdateListView("ListenWaitTime = " + ListenWaitTime);
                                    ListenWaitTime++;
                                    Thread.Sleep(1000);
                                }
                                if (Flag_ConnectionSuccess == true)
                                {
                                    DataConnection dataconnection = new DataConnection(this);
                                    dataconnection.DataClient = Data_listener;
                                    dataconnection.DevicePort = ToOpenDevicePort;
                                    dataconnection.UserID = UserID;
                                    UpdateListView("ToOpenDevicePort = " + ToOpenDevicePort);
                                    ThreadPool.QueueUserWorkItem(new WaitCallback(dataconnection.DoDataProcess));

                                }
                                else
                                {
                                    UpdateListView("클라이언트 접속이 되지 않았습니다. 재접속하여주십시요 ");
                                    Data_listener.Stop();
                                }
                            }
                            catch (SocketException e)
                            {
                                UpdateListView(ToOpenDevicePort + " Port DataListen Socket Exception " + e.ToString());
                                ErrorLoging(e.Message, e.StackTrace);
                            }
                            catch (Exception e)
                            {
                                UpdateListView("Listen Exception " + e.ToString());
                                ErrorLoging(e.Message, e.StackTrace);
                            }
                        }
                        else
                        {
                            UpdateListView(ToOpenDevicePort + " Port는 사용중입니다. 재접속해주십시요.");
                        }
                    }
                    Thread.Sleep(1000);         //통신 연결을 위한 대기 시간 설정
                }
            }
            catch (Exception e)
            {
                UpdateListView("Outter Exception " + e.ToString());
                ErrorLoging(e.Message, e.StackTrace);
            }
            finally
            {
                Data_listener.Stop();
                textBox5.Text = "False";
                UpdateListView("Data Listen Thread 종료.");
                //2018.09.03 ok 추가
                Thread.Sleep(3000);
            }
        }                                       //SQL 접속 스레드 대기 시작
        private string getMyIp()                                                      //Server 컴퓨터 IP 값 확인
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }
        public void UpdateListView(string text)
        {
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            lvi = new ListViewItem();
            lvi.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = text;
            lvi.SubItems.Add(lvsi);
            this.listView1.Items.Add(lvi);
            listView1.Items[listView1.Items.Count - 1].EnsureVisible();

            ////2017.12.12 리스트뷰 로그 추가
            //try { if (Directory.Exists(@"d:\DeviceConnector\") == false) { DirectoryInfo di = Directory.CreateDirectory(@"d:\DeviceConnector\"); } }
            //catch (Exception e) { MessageBox.Show(e.ToString()); return; }

            //StreamWriter sw = new StreamWriter(@"d:\DeviceConnector\" + "EventLog.txt", true, Encoding.Default);//File.CreateText(path + Name);
            //sw.WriteLine(DateTime.Now.ToString() + " : " + text);
            //sw.WriteLine("");
            //sw.Close();
        }
        public void UpdateListView(byte[] text)
        {
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            lvi = new ListViewItem();
            lvi.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = ByteToString(text);
            lvi.SubItems.Add(lvsi);
            this.listView1.Items.Add(lvi);
            listView1.Items[listView1.Items.Count - 1].EnsureVisible();

            ////2017.12.12 리스트뷰 로그 추가
            //try { if (Directory.Exists(@"d:\DeviceConnector\") == false) { DirectoryInfo di = Directory.CreateDirectory(@"d:\DeviceConnector\"); } }
            //catch (Exception e) { MessageBox.Show(e.ToString()); return; }

            //StreamWriter sw = new StreamWriter(@"d:\DeviceConnector\" + "EventLog.txt", true, Encoding.Default);//File.CreateText(path + Name);
            //sw.WriteLine(DateTime.Now.ToString() + " : " + ByteToString(text));
            //sw.WriteLine("");
            //sw.Close();
        }
        public string HexString2Ascii(string hexString)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i <= hexString.Length - 2; i += 2)
            {
                sb.Append(Convert.ToString(Convert.ToChar(Int32.Parse(hexString.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
            }
            return sb.ToString();
        }
        public string FromHexString(string hexString)
        {
            var bytes = new byte[hexString.Length / 2];
            for (var i = 0; i < bytes.Length; i++)
            {
                bytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            }
            return Encoding.Default.GetString(bytes);
        }
        public string ByteToString(byte[] strByte)     // 바이트 -> 스트링
        {
            string str = Encoding.Default.GetString(strByte);
            return str;
        }
        public string ByteArrayToHexString(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder(1024);
            foreach (byte b in bytes)
            {
                sb.AppendFormat("0x{0:x} ", ByteToHex(b));
            }
            return sb.ToString().TrimEnd();
        }           //바이트 -> 스트링
        public string ByteArrayToHexString_NoSpace(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder(1024);
            foreach (byte b in bytes)
            {
                sb.AppendFormat("0x{0:x}", ByteToHex(b));
            }
            return sb.ToString().TrimEnd();
        }           //바이트 -> 스트링
        public string ByteToHex(byte b)
        {
            return Convert.ToString(b, 16).PadLeft(2, '0').ToUpper();
        }//바이트 -> 스트링
        public static string ToHexString(string str)
        {
            var sb = new StringBuilder();
            var bytes = Encoding.ASCII.GetBytes(str);
            foreach (var t in bytes)
            {
                sb.Append(t.ToString("X2"));
            }
            return sb.ToString();
        }
        public bool ScanPortUse(int portnum)        //사용 포트 확인
        {
            //bool inUse = false;
            //string hostname = "10.224.65.23";
            //IPAddress ipa = (IPAddress)Dns.GetHostAddresses(hostname)[0];

            //try
            //{
            //    Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //    sock.Connect(ipa, portnum);
            //    if (sock.Connected == true)  // Port is in use and connection is successful
            //    {
            //        inUse = true;
            //    }
            //    sock.Close();
            //}
            //catch (SocketException ex)
            //{
            //    if (ex.ErrorCode == 10061)  // Port is unused and could not establish connection 
            //        inUse = false;
            //    else
            //        inUse = false;
            //}

            bool inUse = false;
            System.Net.NetworkInformation.IPGlobalProperties ipProperties = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint[] ipEndPoints = ipProperties.GetActiveTcpListeners();

            foreach (IPEndPoint endPoint in ipEndPoints)
            {
                if (endPoint.Port == portnum)
                {
                    inUse = true;
                    break;
                }
            }
            return inUse;
        }
        public void ErrorLoging(string Msg, string Stacktrace)
        {
            RuntimeHelpers.PrepareConstrainedRegions();
            try
            {
                if (Directory.Exists(@"d:\DeviceConnector\") == false)
                {
                    DirectoryInfo di = Directory.CreateDirectory(@"d:\DeviceConnector\");
                }
                StreamWriter sw = new StreamWriter(@"d:\DeviceConnector\" + "ErrorLog.txt", true, Encoding.Default);//File.CreateText(path + Name);
                sw.WriteLine(DateTime.Now.ToString() + " : " + Msg);
                sw.WriteLine(Stacktrace);
                sw.WriteLine("");
                sw.Close();
            }
            catch (Exception e)
            {
                UpdateListView(e.ToString());
                return;
            }


        }

        //2017.12.15----------------------------------------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            int Temp_CloseThread_Port = 0;
            bool Flag_NotFindPort = false;
            if (Int32.TryParse(textBox3.Text, out Temp_CloseThread_Port))
            {
                if (Temp_CloseThread_Port < 10000 || Temp_CloseThread_Port > 65533)
                {
                    //포트 범위초과
                    textBox4.Text = "범위를 초과한 포트입니다.";
                }
                else
                {
                    //요청 포트의 스레드가 있는지 확인
                    for (int i = 0; i < 100; i++)
                    {
                        if (WorkingThread_Port[i] == Temp_CloseThread_Port)
                        {
                            Flag_ClosingThread = true;
                            ClosingThread_Port = Temp_CloseThread_Port;
                            UpdateListView(Temp_CloseThread_Port + " 포트 종료를 요청합니다.");
                            Flag_NotFindPort = false;
                            break;
                        }
                        else
                        {
                            Flag_NotFindPort = true;
                        }
                    }
                    if (Flag_NotFindPort)
                    {
                        UpdateListView("요청하신 " + Temp_CloseThread_Port + " 포트의 스레드가 없습니다.");
                    }
                }
            }
            else
            {
                textBox4.Text = "숫자를 입력하세요";
            }
        }
        //2018.07.03-------------------------------------------------------------------------
        public void WatchDogStart(object obj)
        {
            TcpListener Watchdog_listener = new TcpListener(IPAddress.Any, 9999);
            bool Flag_WathdogConnectionSuccess = false;
            RuntimeHelpers.PrepareConstrainedRegions();
            try
            {
                while (true)
                {
                    short ListenWaitTime = 0;
                    if (!ScanPortUse(9999))
                    {
                        //UpdateListView("Watchdog_listener Open ");
                        RuntimeHelpers.PrepareConstrainedRegions();
                        try
                        {
                            Watchdog_listener = new TcpListener(IPAddress.Any, 9999);
                            Watchdog_listener.Start();
                            while (!Watchdog_listener.Pending())
                            {
                                Flag_WathdogConnectionSuccess = true;
                                if (ListenWaitTime == 21600)
                                {
                                    Flag_WathdogConnectionSuccess = false;
                                    break;
                                }
                                ListenWaitTime++;
                                Thread.Sleep(1000);
                            }
                            if (Flag_WathdogConnectionSuccess == true)
                            {
                                WatchConnection inst_Watchdog = new WatchConnection(this);
                                inst_Watchdog.Watch_Data = Watchdog_listener;
                                ThreadPool.QueueUserWorkItem(new WaitCallback(inst_Watchdog.WatchdogProcess));
                                TextShow("Remote Stop 감시중");
                            }
                            else
                            {
                                //UpdateListView("WatchDog Port 접속이 되지 않았습니다. ");
                                Watchdog_listener.Stop();
                            }
                        }
                        catch (SocketException e)
                        {
                            UpdateListView("Watch-SocketException : " + e.ToString());
                            ErrorLoging(e.Message, e.StackTrace);
                        }
                        catch (Exception e)
                        {
                            UpdateListView("Watch-Exception In ScanPort : " + e.ToString());
                            ErrorLoging(e.Message, e.StackTrace);
                        }
                    }
                    //else
                    //{
                    //    UpdateListView("Watchdog Port 사용중입니다. 다시 접속해 주십시요.");
                    //}
                    Thread.Sleep(1000);         //통신 연결을 위한 대기 시간 설정
                }
            }
            catch (Exception e)
            {
                UpdateListView("Watch Exception Type : " + e.ToString());
                ErrorLoging(e.Message, e.StackTrace);
            }
            finally
            {
                //Data_listener.Stop();
                UpdateListView("Watch Listen Thread 종료.");
                //2018.09.03 ok 추가
                Thread.Sleep(3000);
            }
        }
        public void TextShow(string textMsg)
        {
            textBox6.Text = textMsg;
        }
    }
    public class ClientConnection
    {
        Form1 main;
        NetworkStream ns;
        ASCIIEncoding ascii = new ASCIIEncoding();
        //2016.6.6 ok Modified - connections 제거
        //private int connections = 0;
        private static object PortLock = new object();

        //2018.07.10
        string IDstr = "";
        string PWstr = "";
        string DeviceStr = "";
        string Versionstr = "";

        public ClientConnection(Form1 obj)
        {
            main = obj;
        }
        public TcpListener ConnectClient;
        private IPAddress m_ClientIP;
        public IPAddress ClientIP       //접속한 클라이언트 IP 확인
        {
            get
            {
                return m_ClientIP;
            }
            set
            {
                m_ClientIP = value;
            }
        }
        public void SeperateConnection(object state)
        {
            RuntimeHelpers.PrepareConstrainedRegions();
            try
            {
                ConnectClient.Start();
                Socket client = ConnectClient.AcceptSocket();
                ClientIP = ((IPEndPoint)client.RemoteEndPoint).Address;
                ns = new NetworkStream(client);
                main.UpdateListView(ClientIP.ToString() + " IP Address Connect. " + ((IPEndPoint)client.RemoteEndPoint).Port.ToString() + "Port");
                main.listView1.Items[main.listView1.Items.Count - 1].UseItemStyleForSubItems = false;
                main.listView1.Items[main.listView1.Items.Count - 1].SubItems[1].BackColor = Color.PaleGreen;
                int recv;
                bool Flag_PortUsing = false;
                byte[] Recv_Data = new byte[60];

                ns.ReadTimeout = 5000;
                ns.WriteTimeout = 5000;

                RuntimeHelpers.PrepareConstrainedRegions();
                try
                {

                    while (true)
                    {
                        Recv_Data = new byte[60];
                        recv = ns.Read(Recv_Data, 0, Recv_Data.Length);
                        //main.UpdateListView(BitConverter.ToString(Recv_Data));
                        if (recv == 0)
                        {
                            main.UpdateListView("Receive Data Length = 0");
                            break;
                        }
                        else
                        {
                            int StartIndex = 0;
                            bool Data_Come = false;
                            byte[] tempData = new byte[recv];
                            //Start 값 찾기
                            for (int i = 0; i < recv; i++)
                            {
                                if (Recv_Data[i] != 0x00)
                                {
                                    StartIndex = i; Data_Come = true; break;
                                }
                            }
                            if (Data_Come == true)
                            {
                                Data_Come = false;
                                for (int i = StartIndex; i < recv; i++)
                                {
                                    tempData[i - StartIndex] = Recv_Data[i];
                                }
                                //데이터 유효성 검사
                                ushort Data_Valid = 0;
                                Data_Valid = CheckValidity(tempData);
                                if (Data_Valid == 0)
                                {
                                    byte[] Reply_byte = new byte[3];
                                    Reply_byte[0] = 0x00;
                                    Reply_byte[1] = 0x00;
                                    Reply_byte[2] = 0x00;
                                    ns.Write(Reply_byte, 0, Reply_byte.Length);
                                }
                                else if (Data_Valid == 4)
                                {
                                    byte[] Reply_byte = new byte[3];
                                    Reply_byte[0] = 0x04;
                                    Reply_byte[1] = 0x00;
                                    Reply_byte[2] = 0x00;
                                    ns.Write(Reply_byte, 0, Reply_byte.Length);
                                }
                                else if (Data_Valid == 5)
                                {
                                    byte[] Reply_byte = new byte[3];
                                    Reply_byte[0] = 0x05;
                                    Reply_byte[1] = 0x00;
                                    Reply_byte[2] = 0x00;
                                    ns.Write(Reply_byte, 0, Reply_byte.Length);
                                }
                                else
                                {
                                    for (int i = 0; i < 100; i++)
                                    {
                                        if (main.DevicePort_Number[i] == Data_Valid)
                                        {
                                            byte[] Reply_byte = new byte[3];
                                            Reply_byte[0] = 0x03;
                                            Reply_byte[1] = 0x00;
                                            Reply_byte[2] = 0x00;
                                            ns.Write(Reply_byte, 0, Reply_byte.Length);
                                            main.UpdateListView(Data_Valid + " Port 사용중");
                                            Flag_PortUsing = true;
                                            break;
                                        }
                                    }
                                    //빈 포트 찾기
                                    lock (PortLock)
                                    {
                                        if (!Flag_PortUsing)
                                        {
                                            //byte[] tempbyte = BitConverter.GetBytes(main.OpenPort_Number);
                                            byte[] tempbyte = BitConverter.GetBytes(Data_Valid);
                                            byte[] Reply_byte = new byte[3];
                                            Reply_byte[0] = 0x02;
                                            Reply_byte[1] = tempbyte[1];
                                            Reply_byte[2] = tempbyte[0];
                                            ns.Write(Reply_byte, 0, Reply_byte.Length);
                                            main.UpdateListView("Port : " + Data_Valid + " Open Wait for Client");
                                            main.ToOpenDevicePort = Data_Valid;
                                            main.OpenPort_Number = Data_Valid;
                                            main.UserID = IDstr;
                                            main.Flag_DeviceConnect = true;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                        Thread.Sleep(1000);
                    }
                }
                catch (Exception e)
                {
                    // 2019.03.25
                }
                finally
                {
                    // 2019.03.25
                }
            }
            catch (Exception e)
            {
                main.UpdateListView("Seperate 데이터 대기시간 초과");
                ns.Flush(); ns.Close();
                main.UpdateListView(ClientIP.ToString() + " Disconnect.");
                main.ErrorLoging(e.Message, e.StackTrace);
            }
            finally
            {
                ns.Close();
                //ConnectClient.Stop();
                main.UpdateListView(ClientIP.ToString() + " Disconnect.");
                //포트번호 넘겨주고 재접속용 소켓 오픈 Flag 만들어 다시 서버 소켓 리슨대기
            }
        }

//        private ushort CheckValidity(byte[] data)
        public ushort CheckValidity(byte[] data)
        {
            byte[] ID, PW, DeviceNum, VersionInfo;
            //추후 ID와 PW 를 class 전역변수로 변경하여 main 클래스에 저장하여 HMI 에 표시한다.
            //string IDstr = "";
            //string PWstr = "";
            //string DeviceStr = "";
            //string Versionstr = "";
            ushort value = 0;
            int Carrage_Count = 0;
            //CR : 13 , LF : 10
            RuntimeHelpers.PrepareConstrainedRegions();
            try
            {
                int Password_StartCnt = 0;
                int Device_StartCnt = 0;
                int Version_StartCnt = 0;
                for (int i = 0; i < data.Length; i++)
                {
                    if (data[i] == 0x0d)
                    {
                        Carrage_Count++;
                    }
                }
                if (Carrage_Count < 3)
                {
                    return 5;
                }

                for (int i = 0; i < data.Length; i++)
                {
                    Password_StartCnt++;
                    if (data[i] == 0x0d) //첫번째 CR ... 아이디
                    {
                        ID = new byte[i];
                        for (int k = 0; k < ID.Length; k++)
                        {
                            ID[k] = data[k];
                        }
                        //main.UpdateListView(BitConverter.ToString(data));
                        IDstr = Encoding.Default.GetString(ID);

                        //test를 위해 id message박스 추가
                        MessageBox.Show(IDstr);

                        IDstr = IDstr.Replace("\0", string.Empty);  //  \0 : null
                        break;
                    }
                }
                for (int i = Password_StartCnt; i < data.Length; i++)
                {
                    Device_StartCnt++;
                    if (data[i] == 0x0d) //두번째 CR ... 패스워드
                    {
                        PW = new byte[i - Password_StartCnt];
                        for (int k = 0; k < PW.Length; k++)
                        {
                            PW[k] = data[k + Password_StartCnt];
                        }
                        PWstr = Encoding.Default.GetString(PW);

                        //test를 위해 패스워드 message박스 추가
                        MessageBox.Show(PWstr);

                        PWstr = PWstr.Replace("\0", string.Empty);
                        break;
                    }
                }
                ////ID, PW를 DB 에서 찾기
#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다.
                OracleConnection conn = new OracleConnection(ConfigurationManager.AppSettings["strConn"]);
#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.
//                string sql = @"SELECT * FROM common_user_master where UserID = 'admin' and UserPW =pkg_encrypt_decrypt.encrypt('dfadmin!@')";

                string sql = @"SELECT * FROM common_user_master where UserID = '" + IDstr + @"' and UserPW =pkg_encrypt_decrypt.encrypt('" + PWstr + @"')";
                DataSet ds = new DataSet();
#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다.
                OracleDataAdapter adpt = new OracleDataAdapter(sql, conn);
#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.

                adpt.Fill(ds, "common_user_master");
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        main.UpdateListView("ID : " + IDstr + ", Password : " + PWstr);
                    }
                    else
                    {
                        main.UpdateListView("ID : " + IDstr + ", Password : " + PWstr);
                        main.UpdateListView("Access Denied. Check ID and Password.");
                        conn.Close();
                        adpt.Dispose();
                        ds.Dispose();
                        return 0;
                    }
                }
                else
                {
                    main.UpdateListView("일치하는 Database가 없습니다.");
                    conn.Close();
                    adpt.Dispose();
                    ds.Dispose();
                    return 0;
                }
                conn.Close();
                adpt.Dispose();
                ds.Dispose();

                for (int i = Password_StartCnt + Device_StartCnt; i < data.Length; i++)
                {
                    Version_StartCnt++;
                    if (data[i] == 0x0d) //세번째 CR ... 디바이스 넘버
                    {
                        DeviceNum = new byte[i - Password_StartCnt - Device_StartCnt];
                        for (int k = 0; k < DeviceNum.Length; k++)
                        {
                            DeviceNum[k] = data[k + Password_StartCnt + Device_StartCnt];
                        }
                        DeviceStr = Encoding.Default.GetString(DeviceNum);
                        DeviceStr = DeviceStr.Replace("\0", string.Empty);
                        break;
                    }
                }

                for (int i = Password_StartCnt + Device_StartCnt + Version_StartCnt; i < data.Length; i++)
                {
                    if (data[i] == 0x0d) //세번째 CR ... 디바이스 넘버
                    {
                        VersionInfo = new byte[i - Password_StartCnt - Device_StartCnt + Version_StartCnt];
                        for (int k = 0; k < VersionInfo.Length; k++)
                        {
                            VersionInfo[k] = data[k + Password_StartCnt + Device_StartCnt + Version_StartCnt];
                        }
                        Versionstr = Encoding.Default.GetString(VersionInfo);
                        Versionstr = Versionstr.Replace("\0", string.Empty);
                        break;
                    }
                }

                ////장비의 TCP Port를 DB에서 읽어옴
#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다.
                OracleConnection conn2 = new OracleConnection(ConfigurationManager.AppSettings["strConn"]);
#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.
                DataSet ds2 = new DataSet();
                string sql2 = @"SELECT TCPPort FROM fc_master where DeviceNum = '" + DeviceStr + "' ";
#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다.
                OracleDataAdapter adpt2 = new OracleDataAdapter(sql2, conn2);
#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.

                adpt2.Fill(ds2, "fc_master");
                if (ds2.Tables.Count > 0)
                {
                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow r2 in ds2.Tables[0].Rows)
                        {
                            value = Convert.ToUInt16(r2["TCPPort"]);
                        }
                        main.UpdateListView("DeviceNum : " + DeviceStr + ". TCP Port : " + value);
                    }
                    else
                    {
                        main.UpdateListView(DeviceStr + " 디바이스를 찾을 수 없습니다.");
                        conn2.Close();
                        adpt2.Dispose();
                        ds2.Dispose();
                        return 4;
                    }
                }
                else
                {
                    main.UpdateListView("NO Table Error");
                    conn2.Close();
                    adpt2.Dispose();
                    ds2.Dispose();
                    return 4;
                }
                conn2.Close();
                adpt2.Dispose();
                ds2.Dispose();

            }
            catch (Exception e)
            {
                main.UpdateListView(e.ToString());
                main.ErrorLoging(e.Message, e.StackTrace);
                return 0;
            }
            return value;
        }
        public string ByteArrayToHexString(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder(1024);
            foreach (byte b in bytes)
            {
                sb.AppendFormat("{0} ", ByteToHex(b));
            }
            return sb.ToString().TrimEnd();
        }           //바이트 -> 스트링
        public static string ByteToHex(byte b)  //바이트 -> 스트링
        {
            return Convert.ToString(b, 16).PadLeft(2, '0').ToUpper();
        }
    }
    public class DataConnection
    {
        Form1 main;
        public UInt16 DevicePort;
        public DataConnection(Form1 obj)
        {
            main = obj;
        }
        public TcpListener DataClient;
        private IPAddress m_ClientIP;
        public IPAddress ClientIP       //접속한 클라이언트 IP 확인
        {
            get
            {
                return m_ClientIP;
            }
            set
            {
                m_ClientIP = value;
            }
        }
        int myConnectionNum = 0;

        //2017.12.15----------------------------------------------------------------------------------
        private static object ThreadCntLock = new object();
        private static object ThreadEndFlagLock = new object();
        bool Flag_ThreadRunning = true;
        int MyThreadCnt = 0;

        //2018.07.10----------------------------------------------------------------------------------
        public string UserID;
        public void DoDataProcess(object state)
        {
            int temp = 0;
            DataClient.Server.ReceiveTimeout = 15000;
            DataClient.Server.SendTimeout = 15000;
            DataClient.Start();

            bool disconnectCheck = true;

            RuntimeHelpers.PrepareConstrainedRegions();
            try
            {
                Socket Client_Sck = DataClient.AcceptSocket();
                Thread.Sleep(1000);

                ClientIP = ((IPEndPoint)Client_Sck.RemoteEndPoint).Address;

                NetworkStream ns, Device_ns, Temp_ns;
                ns = new NetworkStream(Client_Sck);
                byte[] Temp_ClientData = new byte[30];
                RuntimeHelpers.PrepareConstrainedRegions();
                try
                {
                    //2017.12.15----------------------------------------------------------------------------------
                    lock (ThreadCntLock)
                    {
                        MyThreadCnt = main.ThreadCnt;
                        main.WorkingThread_ID[main.ThreadCnt] = Thread.CurrentThread.ManagedThreadId;
                        main.WorkingThread_Port[main.ThreadCnt] = DevicePort;
                        main.UpdateListView(main.ThreadCnt + " Thread Start. Port = " + DevicePort + ", ThreadID = " + Thread.CurrentThread.ManagedThreadId);
                        main.ThreadCnt++;
                        if (main.ThreadCnt == 100)
                        {
                            main.ThreadCnt = 0;
                        }
                    }
                    //2017.12.15----------------------------------------------------------------------------------

                    main.UpdateListView(ClientIP.ToString() + " IP Address Connect. " + ((IPEndPoint)Client_Sck.RemoteEndPoint).Port.ToString() + "Port");
                    main.listView1.Items[main.listView1.Items.Count - 1].UseItemStyleForSubItems = false;
                    main.listView1.Items[main.listView1.Items.Count - 1].SubItems[1].BackColor = Color.LightBlue;
                }
                catch (Exception ex)
                {
                    main.UpdateListView("Inner Exception " + ex.ToString());
                    main.ErrorLoging(ex.Message, ex.StackTrace);
                }
                //디바이스 서버 접속
                TcpClient DeviceSvr_Open = new TcpClient();
                RuntimeHelpers.PrepareConstrainedRegions();
                try
                {
                    //수정필요
                    DeviceSvr_Open = new TcpClient("112.220.92.6", 65531);
                }
                catch (Exception ex)
                {
                    main.ErrorLoging("Device Server Socket : " + ex.Message, ex.StackTrace);
                    main.UpdateListView("Device Server Socket : " + ex.ToString());
                    ns.Flush(); ns.Close();
                    Client_Sck.Close();
                    DeviceSvr_Open.Close();
                    DataClient.Stop();
                    main.UpdateListView(ClientIP.ToString() + " Disconnect. [ " + main.connections + " ] Connection Active");
                    return;
                }
                main.UpdateListView(" Device Server Open 요청");
                Temp_ns = DeviceSvr_Open.GetStream();
                //main.UpdateListView(" Device Server Open ------------");
                Temp_ns.ReadTimeout = 5000;
                Temp_ns.WriteTimeout = 5000;

                for (int i = 0; i < 1000; i++)
                {
                    if (main.ScanPortUse(main.DeviceOpenPort_Number))
                    {
                        main.DeviceOpenPort_Number++;
                        if (main.DeviceOpenPort_Number >= 63999) main.DeviceOpenPort_Number = 63000;
                    }
                    else
                    {
                        main.DeviceOpenPort_Number++;
                        byte[] tempbyte = BitConverter.GetBytes(main.DeviceOpenPort_Number);
                        byte[] Reply_byte = new byte[3];
                        temp = main.DeviceOpenPort_Number;
                        Reply_byte[0] = 0x55;
                        Reply_byte[1] = tempbyte[1];
                        Reply_byte[2] = tempbyte[0];
                        Temp_ns.Write(Reply_byte, 0, Reply_byte.Length);
                        main.UpdateListView(BitConverter.ToString(Reply_byte) + "  DeviceOpenPort : " + main.DeviceOpenPort_Number + " Open Wait for Client");
                        break;
                    }
                }
                DeviceSvr_Open.Close();
                Thread.Sleep(3000);

                //디바이스 서버 데이터 받기
                TcpClient DeviceSvr_Listen = new TcpClient();
                RuntimeHelpers.PrepareConstrainedRegions();
                try
                {
                    //수정필요
                    DeviceSvr_Listen = new TcpClient("112.220.92.6", temp);
                }
                catch (Exception ex)
                {
                    main.ErrorLoging("Device Listen Socket : " + ex.Message, ex.StackTrace);
                    main.UpdateListView("Device Listen Socket : " + ex.ToString());
                    ns.Flush(); ns.Close();
                    Client_Sck.Close();
                    DeviceSvr_Listen.Close();
                    main.UpdateListView(ClientIP.ToString() + " Disconnect. [ " + main.connections + " ] Connection Active");
                    return;
                }
                Device_ns = DeviceSvr_Listen.GetStream();
                main.UpdateListView("Device Server Connected ");
                main.listView1.Items[main.listView1.Items.Count - 1].UseItemStyleForSubItems = false;
                main.listView1.Items[main.listView1.Items.Count - 1].SubItems[1].BackColor = Color.Coral;

                int Recv_ClientData_Len = 0;
                int Recv_DeviceData_Len = 0;
                byte[] Recv_ClientData = new byte[2000];
                byte[] Recv_ClientData_ToDevSvr = new byte[2002];
                byte[] Recv_DeviceData = new byte[5000];
                bool Flag_PortDataReceive = false;
                RuntimeHelpers.PrepareConstrainedRegions();
                try
                {
                    //2017.12.17
                    main.connections++;
                    //2016.6.6 ok Modified  1초로 줄임
                    Thread.Sleep(1000);

                    ns.ReadTimeout = 15000;
                    ns.WriteTimeout = 15000;
                    Device_ns.ReadTimeout = 15000;
                    Device_ns.WriteTimeout = 15000;
                    main.UpdateListView("DevicePort = " + DevicePort);

                    //2018.07.03
//#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다.
//                    OracleConnection conn3 = new OracleConnection(ConfigurationManager.AppSettings["strConn"]);
//#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.

//                    conn3.Open();
//                    string commandText = @"MERGE INTO fc_realtime_connected USING  DUAL ON ( devicenum = '" + DevicePort.ToString() + "' )"
//                                        + @"WHEN MATCHED THEN UPDATE SET UserID = '" + UserID + @"', intime = TO_CHAR(SYSDATE,'YYYY-MM-DD HH24:MI:SS'), Connected = 'Y' "
//                                        + @"WHEN NOT MATCHED THEN Insert (UserID, DeviceNum, InTime, Connected) values ('" + UserID + @"', '" + DevicePort.ToString() + @"', TO_CHAR(SYSDATE,'YYYY-MM-DD HH24:MI:SS'), 'Y' );";
//#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다.
//                    OracleCommand cmd3 = new OracleCommand(commandText, conn3);
//#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.
//                    cmd3.ExecuteNonQuery();
//                    conn3.Close();

#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다.
                    OracleCommand cmd3 = new OracleCommand("SP_FC_CONNECTED_YES", new OracleConnection(ConfigurationManager.AppSettings["strConn"]));
#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.
                    cmd3.CommandType = CommandType.StoredProcedure;

                    cmd3.Parameters.Add(new OracleParameter("iv_UserId", UserID));
                    cmd3.Parameters.Add(new OracleParameter("iv_DeviceNum", DevicePort.ToString()));

                    cmd3.Connection.Open();
                    cmd3.ExecuteNonQuery();
                    cmd3.Connection.Close();


                    while (Flag_ThreadRunning)
                    {
                        Recv_ClientData_Len = ns.Read(Recv_ClientData, 0, 2000);
                        if (Recv_ClientData_Len == 0)
                        {
                            main.UpdateListView("Recv_ClientData Length = 0");
                            break;
                        }
                        else
                        {
                            //--------------------------------------------------------------------------------------
                            //Read 하고자 하는 디바이스 포트 번호 전송 필요
                            for (int i = 0; i < Recv_ClientData_Len; i++)
                            {
                                Recv_ClientData_ToDevSvr[i + 2] = Recv_ClientData[i];
                            }
                            byte[] PortNum = BitConverter.GetBytes(DevicePort);
                            Recv_ClientData_ToDevSvr[0] = PortNum[0];
                            Recv_ClientData_ToDevSvr[1] = PortNum[1];


                            Device_ns.Write(Recv_ClientData_ToDevSvr, 0, Recv_ClientData_Len + 2);
                            //int ToDevice_DataLen = Device_Sck.Send(Recv_ClientData);

                            //main.UpdateListView("Send. Write to Device Server : " + main.ByteArrayToHexString(Recv_ClientData_ToDevSvr));
                            //main.UpdateListView(Encoding.UTF8.GetString(Recv_ClientData_ToDevSvr));
                            Recv_ClientData = new byte[2000];
                            Recv_ClientData_ToDevSvr = new byte[2002];
                            //------------------------------------------------------------------------------------------
                            Recv_DeviceData_Len = Device_ns.Read(Recv_DeviceData, 0, Recv_DeviceData.Length);
                            //Recv_DeviceData_Len = Device_Sck.Receive(Recv_DeviceData);
                            //main.UpdateListView(Recv_DeviceData_Len + " from Device Server.. " + main.ByteArrayToHexString(Recv_DeviceData));
                            //main.UpdateListView(Encoding.UTF8.GetString(Recv_DeviceData));
                            //-------------------------------------------------------------------------------------------
                            ns.Write(Recv_DeviceData, 0, Recv_DeviceData_Len);
                            //int ToClient_DataLen = Device_Sck.Send(Recv_DeviceData);

                            //main.UpdateListView(Recv_DeviceData_Len + " ]. Write to Client : " + main.ByteArrayToHexString(Recv_DeviceData));
                            //main.UpdateListView(Encoding.UTF8.GetString(Recv_DeviceData));
                            Recv_DeviceData = new byte[5000];
                            //2016.6.6 ok Modified
                            //포트 정보 한번 받으면 그 데이터를 저장해두기
                            if (Flag_PortDataReceive == false)
                            {
                                Flag_PortDataReceive = true;
                                //main.connections 에 맞는 데이터 추가
                                main.DevicePort_Number[main.connections] = DevicePort;
                                myConnectionNum = main.connections;

                                main.UpdateListView("[ " + main.connections + " ] 접속중");
                                main.textBox2.Text = ((short)(main.connections)).ToString();
                            }

                            //2017.12.15----------------------------------------------------------------------------------
                            if (main.Flag_ClosingThread) //스레드 닫기 버튼이 눌러졌는지 확인
                            {
                                if (main.ClosingThread_Port == DevicePort)  //현재 요청 스레드 닫기 버튼이 같은 포트 인지 확인
                                {
                                    //닫기
                                    lock (ThreadEndFlagLock)
                                    {
                                        main.Flag_ClosingThread = false;
                                    }
                                    Flag_ThreadRunning = false;
                                    main.UpdateListView(DevicePort + " Port  Thread가 곧 종료됩니다. ");
                                }
                                else
                                {
                                    // Do nothing
                                }
                            }
                            //2017.12.15----------------------------------------------------------------------------------
                            //2018.07.09
                            if (main.Flag_ProgramStop)
                            {
                                Flag_ThreadRunning = false;
                                //main.UpdateListView(DevicePort + " Port  Thread가 곧 종료됩니다. ");
                            }
                        }
                    }
                }
                catch (IOException ex)
                {
                    main.UpdateListView("IO Exception 대기시간 초과 : " + ex.ToString());
                    main.ErrorLoging("IO Exception 대기시간 초과 : " + ex.Message, ex.StackTrace);

                    disconnectCheck = false;
                }
                catch (Exception ex)
                {
                    main.UpdateListView("DoProcess " + ex.ToString());
                    main.ErrorLoging("DoProcess " + ex.Message, ex.StackTrace);

                    disconnectCheck = false;
                }
                finally
                {
                    //2017.12.15----------------------------------------------------------------------------------
                    MyThreadCnt = main.ThreadCnt;
                    main.WorkingThread_ID[MyThreadCnt] = 0;
                    main.WorkingThread_Port[MyThreadCnt] = 0;
                    //2017.12.15----------------------------------------------------------------------------------

                    ns.Flush(); ns.Close();
                    Client_Sck.Close();
                    Device_ns.Flush(); Device_ns.Close();
                    DataClient.Stop();
                    DeviceSvr_Listen.Close();

                    //2016.6.6 ok Modified
                    if (main.connections > 0) main.connections--;

                    //2017.12.11 포트번호로 찾아서 배열에서 제거하는 로직으로 변경
                    //main.DevicePort_Number[myConnectionNum] = 0;
                    for (int i = 0; i < 30; i++)
                    {
                        if (main.DevicePort_Number[i] == DevicePort)
                        {
                            main.DevicePort_Number[i] = 0;
                        }
                    }
                    //----------------------------------------------------------------

                    main.UpdateListView("[ " + main.connections + " ] 접속중");
                    main.textBox2.Text = ((short)(main.connections)).ToString();
                    //2016.6.21 ok 버그 수정
                    for (int i = 0; i < 100; i++)
                    {
                        if (i != 99)
                        {
                            if (main.DevicePort_Number[i] == 0)
                            {
                                main.DevicePort_Number[i] = main.DevicePort_Number[i + 1];
                                main.DevicePort_Number[i + 1] = 0;
                            }
                        }
                        else
                        {
                            main.DevicePort_Number[98] = main.DevicePort_Number[99];
                            main.DevicePort_Number[99] = 0;
                        }
                    }
                    //2016.6.6 ok Modified - main.connections 로 변경
                    main.UpdateListView("[" + myConnectionNum + "] : " + ClientIP.ToString() + " Disconnect. [ " + main.connections + " ] Connection Active");
                    main.UpdateListView(ClientIP.ToString() + " : Device Server Disconnected ");
                    main.listView1.Items[main.listView1.Items.Count - 1].UseItemStyleForSubItems = false;
                    main.listView1.Items[main.listView1.Items.Count - 1].SubItems[1].BackColor = Color.Cyan;
                }
            }
            catch (Exception ex)
            {
                main.UpdateListView("DoProcess " + ex.ToString());
                main.ErrorLoging("DoProcess " + ex.Message, ex.StackTrace);

                disconnectCheck = false;
            }
            finally
            {
                DataClient.Stop();
                main.UpdateListView("[" + myConnectionNum + "] : " + ClientIP.ToString() + " Disconnect. [ " + main.connections + " ] Connection Active");
                main.UpdateListView(ClientIP.ToString() + " : Device Server Disconnected ");
                main.listView1.Items[main.listView1.Items.Count - 1].UseItemStyleForSubItems = false;
                main.listView1.Items[main.listView1.Items.Count - 1].SubItems[1].BackColor = Color.Cyan;

                //2018.09.03 ok 추가
                Thread.Sleep(3000);
                //2018.07.09
                //MySQL Data 추가 코드 작성
                if (disconnectCheck)
                {
                    //                    //                    string strConn3 = "Server=112.220.92.5; Database=device_realtime; Uid=dfadmin; Pwd=dfadmin!@;";
                    //                    string strConn = "Data Source= (DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 10.224.65.23)(PORT = 1521))) (CONNECT_DATA = (SERVICE_NAME = FCPMPRD)(SERVER = DEDICATED))) ;USER ID=DFMONITORING;PASSWORD=dfadmin!@";
                    //                    OracleConnection conn3 = new OracleConnection(strConn);
                    //                    conn3.Open();
                    //                    string commandText = @"Insert into fc_realtime_connected (UserID, DeviceNum, OutTime, Connected) values ('"
                    //                                                            + UserID + @"', '" + DevicePort.ToString() + @"', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @"', 'N' "
                    //                                                            + @") on duplicate key update UserID = '" + UserID + @"',  OutTime = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @"', Connected = 'N'";
                    ////                    MySqlCommand cmd3 = new MySqlCommand(commandText, conn3);
                    //                    OracleCommand cmd3 = new OracleCommand(commandText, conn3);
                    //                    cmd3.ExecuteNonQuery();
                    //                    conn3.Close();

#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다.
                    OracleCommand cmd3 = new OracleCommand("SP_FC_CONNECTED_NO", new OracleConnection(ConfigurationManager.AppSettings["strConn"]));
#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.
                    cmd3.CommandType = CommandType.StoredProcedure;

                    cmd3.Parameters.Add(new OracleParameter("iv_UserId", UserID));
                    cmd3.Parameters.Add(new OracleParameter("iv_DeviceNum", DevicePort.ToString()));
                    cmd3.Parameters.Add(new OracleParameter("iv_Status", "1"));

                    cmd3.Connection.Open();
                    cmd3.ExecuteNonQuery();
                    cmd3.Connection.Close();


                }
                else
                {
                    ////                    string strConn3 = "Server=112.220.92.5; Database=device_realtime; Uid=dfadmin; Pwd=dfadmin!@;";
                    //                    string strConn3 = "Data Source= (DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 10.224.65.23)(PORT = 1521))) (CONNECT_DATA = (SERVICE_NAME = FCPMPRD)(SERVER = DEDICATED))) ;USER ID=DFMONITORING;PASSWORD=dfadmin!@";
                    ////                    MySqlConnection conn3 = new MySqlConnection(strConn3);
                    //                    OracleConnection conn3 = new OracleConnection(strConn3);
                    //                    conn3.Open();
                    //                    string commandText = @"Insert into fc_realtime_connected (UserID, DeviceNum, Connected) values ('"
                    //                                                            + UserID + @"', '" + DevicePort.ToString() + @"', 'N' "
                    //                                                            + @") on duplicate key update UserID = '" + UserID + @"', Connected = 'N'";
                    //                    //                    MySqlCommand cmd3 = new MySqlCommand(commandText, conn3);
                    //                    OracleCommand cmd3 = new OracleCommand(commandText, conn3);
                    //                    cmd3.ExecuteNonQuery();
                    //                    conn3.Close();

#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다.
                    OracleCommand cmd3 = new OracleCommand("SP_FC_CONNECTED_NO", new OracleConnection(ConfigurationManager.AppSettings["strConn"]));
#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.
                    cmd3.CommandType = CommandType.StoredProcedure;

                    cmd3.Parameters.Add(new OracleParameter("iv_UserId", UserID));
                    cmd3.Parameters.Add(new OracleParameter("iv_DeviceNum", DevicePort.ToString()));
                    cmd3.Parameters.Add(new OracleParameter("iv_Status", "0"));

                    cmd3.Connection.Open();
                    cmd3.ExecuteNonQuery();
                    cmd3.Connection.Close();

                }
            }
        }
    }
    //2018.07.03
    public class WatchConnection
    {
        Form1 main;
        public WatchConnection(Form1 obj)
        {
            main = obj;
        }
        public TcpListener Watch_Data;
        private IPAddress m_ClientIP;
        public IPAddress ClientIP
        {
            get
            {
                return m_ClientIP;
            }
            set
            {
                m_ClientIP = value;
            }
        }
        public void WatchdogProcess(object state)
        {
            RuntimeHelpers.PrepareConstrainedRegions();
            try
            {
                Socket Client_Sck = Watch_Data.AcceptSocket();
                Thread.Sleep(1000);
                NetworkStream ns;
                int Recv_WatchData_Len = 0;
                byte[] Recv_WatchData = new byte[10];
                byte[] Write_WatchData = new byte[10];
                byte[] OrderNum;
                UInt16 OrderNumber;

                ClientIP = ((IPEndPoint)Client_Sck.RemoteEndPoint).Address;
                ns = new NetworkStream(Client_Sck);
                main.UpdateListView("Watchdog " + ClientIP.ToString() + " Connect. " + ((IPEndPoint)Client_Sck.RemoteEndPoint).Port.ToString() + "Port");
                main.listView1.Items[main.listView1.Items.Count - 1].UseItemStyleForSubItems = false;
                main.listView1.Items[main.listView1.Items.Count - 1].SubItems[1].BackColor = Color.PaleGreen;

                RuntimeHelpers.PrepareConstrainedRegions();
                try
                {
                    ns.ReadTimeout = 10000;
                    ns.WriteTimeout = 10000;
                    while (true)
                    {
                        Recv_WatchData_Len = ns.Read(Recv_WatchData, 0, 10);
                        if (Recv_WatchData_Len == 0)
                        {
                            main.UpdateListView("Recv_WatchData_Length = 0");
                            break;
                        }
                        else
                        {
                            main.TextShow("Remote Stop 작동");
                            OrderNum = new byte[2];
                            OrderNum[0] = Recv_WatchData[0];
                            OrderNum[1] = Recv_WatchData[1];
                            OrderNumber = BitConverter.ToUInt16(OrderNum, 0);
                            main.UpdateListView("Num [ " + OrderNumber + " ] Number");

                            Write_WatchData[0] = 0x45;
                            Write_WatchData[1] = 0x58;
                            Write_WatchData[2] = 0x49;
                            Write_WatchData[3] = 0x54;
                            Write_WatchData[4] = 0x31;
                            ns.Write(Write_WatchData, 0, 10);

                            if (OrderNumber == 10 && Recv_WatchData[2] == 0x45 && Recv_WatchData[3] == 0x58
                                && Recv_WatchData[4] == 0x49 && Recv_WatchData[5] == 0x54)
                            //if (OrderNumber == 10)
                            {
                                main.Flag_ProgramStop = true;
                            }
                            Recv_WatchData = new byte[10];
                            Thread.Sleep(1000);
                            break;
                        }
                    }
                }
                catch (IOException e)
                {
                    main.UpdateListView("Watchdog IO Exception");
                    main.ErrorLoging("Watchdog IO Exception : " + e.Message, e.StackTrace);
                }
                catch (Exception e)
                {
                    main.UpdateListView("Watchdog Exception : " + e.ToString());
                    main.ErrorLoging("Watchdog Exception : " + e.Message, e.StackTrace);
                }
                finally
                {
                    ns.Flush(); ns.Close();
                    Client_Sck.Close();
                    Watch_Data.Stop();
                }
            }
            catch (Exception e)
            {
                main.UpdateListView("Outter Exception " + e.ToString());
                main.ErrorLoging("Outter Exception " + e.Message, e.StackTrace);
            }
            finally
            {
                Watch_Data.Stop();
                main.TextShow("Watchdog Thread 종료");
                //2018.09.03 ok 추가
                Thread.Sleep(3000);
            }
        }
    }
}
