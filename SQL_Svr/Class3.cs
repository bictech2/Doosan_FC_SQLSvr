﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_Svr
{
    public abstract class SecurityAlgorithm 
    {
        protected readonly Dictionary<int, int> alphabet;

        public SecurityAlgorithm()
        {
            alphabet = new Dictionary< int, int>();
            //char c = '0';
            alphabet.Add(0, 0);

            for (int i = 1; i < 128; i++)
            {
                alphabet.Add( i, i);
            }
        }

        //public SecurityAlgorithm()
        //{
        //    alphabet = new Dictionary<char, int>();
        //    char c = 'a';
        //    alphabet.Add(c, 0);

        //    for (int i = 1; i < 26; i++)
        //    {
        //        alphabet.Add(++c, i);
        //    }
        //}

        public abstract string Encrypt(string plainText);

        public abstract string Decrypt(string cipher);
    }

}
