﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using RestSharp;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Data;


namespace SQL_Svr
{

    public class DataPase
    {
        public string name;
        public string phone;
    }

    class Node_API
    {
        //select 
        public IRestResponse Node_Api_Post(string qry)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Ssl3 | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls11;

            var client = new RestClient("https://fcpweb.doosan.com:8001");
            //          var client = new RestClient("http://localhost:8001");

            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", qry, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            //Console.WriteLine(response.Content);


                return response;
        }


        //insert / update / create 
        public IRestResponse Node_Api_Put(string qry)
        {
            var client = new RestClient("https://fcpweb.doosan.com:8001/dfexecqry/");
            //           var client = new RestClient("http://localhost:8001/dfexecqry/");

            client.Timeout = -1;

            // DDI 정책상 put과 delete는 공격으로 인식한다 하여 get으로 변경
//            var request = new RestRequest(Method.PUT);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json;");
            request.AddParameter("application/json", qry, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request); 
            //Console.WriteLine(response.Content);

                return response;
        }

        //delete 
        public IRestResponse Node_Api_Del(string qry)
        {
            var client = new RestClient("https://fcpweb.doosan.com:8001/dfexecqry/");
            //            var client = new RestClient("http://localhost:8001/dfexecqry/");

            client.Timeout = -1;
            // DDI 정책상 put과 delete는 공격으로 인식한다 하여 get으로 변경
            //            var request = new RestRequest(Method.DELETE );
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", qry, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);

            return (response);
        }

        public DataSet Parse_Json(IRestResponse res )
        {
            //DataTable Dt = new DataTable();
            DataSet ds = new DataSet();

            if (res.StatusCode == HttpStatusCode.OK)
            {
                ds = JsonConvert.DeserializeObject<DataSet>(res.Content );
            }
            else
            {
                ds.DataSetName = "ERROR(" + res.Content + ")" ;
                ds = JsonConvert.DeserializeObject<DataSet>(res.Content);
            }

            return ds;

        }


    }
}
