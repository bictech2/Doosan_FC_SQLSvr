﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO;

namespace Watch_SQLSvr
{
    public partial class Form1 : Form
    {
        #region DLL Import

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, uint Msg, int wParam, int lParam);
        [DllImport("User32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder lpWindowText, int nMaxCount);
        [DllImport("User32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("User32.dll", SetLastError = true)]
        public static extern IntPtr GetWindow(IntPtr hWnd, uint uCmd);
        [DllImport("User32.dll", SetLastError = true)]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessID);
        [DllImport("User32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassNmae, int nMaxCount);
        #endregion
        public Form1()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;        //Debug 모드에서 크로스 스레드 작업 오류 제거
            Thread hThread = new Thread(delegate () { MoninotringProcess(); });
            hThread.Priority = ThreadPriority.Lowest;
            hThread.IsBackground = true;
            hThread.Start();
        }
        public void MoninotringProcess()
        {
            bool bThreadAlive = true;
            while (bThreadAlive)
            {
                Process[] process = System.Diagnostics.Process.GetProcessesByName("SQL_Svr_Comm");
                if (process.Length <= 0)
                {
                    Thread.Sleep(1000);
                    UpdateListView("SQL_Svr_Comm.exe 파일 재실행");
                    RunProgram();
                }
                else
                {
                    foreach (Process Pr in process)
                    {
                        if (!Pr.EnableRaisingEvents)
                        {
                            Pr.Exited += ProcessExited;
                            Pr.EnableRaisingEvents = true;
                        }
                        KillErrorProcess("SQL_Svr", Pr.Id);
                    }
                }
                Thread.Sleep(5000);
            }
        }
        public void RunProgram()
        {
            Process p1 = new Process();
            //p1.StartInfo.FileName = @"D:\네오피스\프로젝트\2018년 이전\다중통신프로토콜_2017_1208\SQL_Svr\SQL_Svr\bin\Release\SQL_Svr_Comm.exe";
            p1.StartInfo.FileName = @"D:\DeviceConnector\SQL_Svr_Comm.exe";
            p1.EnableRaisingEvents = true;
            p1.Exited += new EventHandler(ProcessExited);
            p1.Start();
            UpdateListView("SQL Server Program을 실행합니다");
            StartLoging();
        }
        public void ProcessExited(object source, EventArgs e)
        {
            //UpdateListView("Process Exited");
        }
        public void KillErrorProcess(String wndTitle, int ValidprocessID)
        {
            uint processID = 0;
            StringBuilder Title = new StringBuilder(256);
            StringBuilder lpClassName = new StringBuilder(256);
            IntPtr tempHwnd = FindWindow(null, null);
            while (tempHwnd.ToInt32() != 0)
            {
                tempHwnd = GetWindow(tempHwnd, 2);   //GW_HWNDNEWT   2
                GetWindowText(tempHwnd, Title, Title.Capacity + 1);
                if (Title.ToString().IndexOf(wndTitle) >= 0)
                {
                    GetWindowThreadProcessId(tempHwnd, out processID);
                    if (processID != ValidprocessID)
                    {
                        GetClassName(tempHwnd, lpClassName, lpClassName.Capacity + 1);
                        if (lpClassName.ToString().CompareTo("#32770") == 0)
                        {
                            SendMessage(tempHwnd, 0x0010, -1, -1);
                            UpdateListView("SQL Server Exited");
                        }
                    }
                }
            }
        }
        public void UpdateListView(string text)
        {
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            lvi = new ListViewItem();
            lvi.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = text;
            lvi.SubItems.Add(lvsi);
            this.listView1.Items.Add(lvi);
            listView1.Items[listView1.Items.Count - 1].EnsureVisible();
        }
        public string ByteToHex(byte b)
        {
            return Convert.ToString(b, 16).PadLeft(2, '0').ToUpper();
        }//바이트 -> 스트링
        public void ErrorLoging(string Msg, string Stacktrace)
        {
            try { if (Directory.Exists(@"d:\DeviceConnector\") == false) { DirectoryInfo di = Directory.CreateDirectory(@"d:\DeviceConnector\"); } }
            catch (Exception e) { MessageBox.Show(e.ToString()); return; }

            StreamWriter sw = new StreamWriter(@"d:\DeviceConnector\" + "Watch_ErrorLog.txt", true, Encoding.Default);//File.CreateText(path + Name);
            sw.WriteLine(DateTime.Now.ToString() + " : " + Msg);
            sw.WriteLine(Stacktrace);
            sw.WriteLine("");
            sw.Close();
        }
        public void StartLoging()
        {
            try { if (Directory.Exists(@"d:\DeviceConnector\") == false) { DirectoryInfo di = Directory.CreateDirectory(@"d:\DeviceConnector\"); } }
            catch (Exception e) { MessageBox.Show(e.ToString()); return; }

            StreamWriter sw = new StreamWriter(@"d:\DeviceConnector\" + "StartLog.txt", true, Encoding.Default);//File.CreateText(path + Name);
            sw.WriteLine(DateTime.Now.ToString() + " : Started");
            sw.WriteLine("");
            sw.Close();
        }
    }
}
